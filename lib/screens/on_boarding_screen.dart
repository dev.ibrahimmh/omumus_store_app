import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/on_boaarding_indicator.dart';
import 'package:omumus_store_app/widgets/on_bording_content.dart';
import 'package:omumus_store_app/widgets/store_text.dart';

class OnBoardingScreen extends StatefulWidget {
  const OnBoardingScreen({Key? key}) : super(key: key);

  @override
  _OnBoardingScreenState createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  late PageController _pageController;
  int _currentPage = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _pageController = PageController();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BACKGROUND_SPLASH,
      body: Stack(
        children: [
          PageView(
            controller: _pageController,
            onPageChanged: (int currentPage) {
              setState(() {
                _currentPage = currentPage;
              });
            },
            children: [
              OnBoardingContent(
                imagePath: 'images/suit.png',
                title: 'Great deals',
                details:
                    'Don\'t miss out our flash sale and \n daily specials with great \n discounts',
              ),
              OnBoardingContent( 
                imagePath: 'images/sports.png',
                title: 'High Quality',
                details:
                    'You can only find high-quality and\n affordable clothing for your\n sports',
              ),
              OnBoardingContent(
                imagePath: 'images/medical_glasses.png',
                title: 'Invite friends',
                details:
                    'You can invite your best friends\n and share with them your first\n order',
              ),
            ],
          ),
          Positioned(
            bottom: SizeConfig.scaleHeight(45),
            left: SizeConfig.scaleWidth(32),
            right: SizeConfig.scaleWidth(32),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Visibility(
                  visible: _currentPage != 2,
                  child: TextButton(
                    onPressed: () {
                      Get.offNamed('/sign_in_screen');
                    },
                    child: StoreText(
                      text: 'SKIP',
                      textColor: AppColors.on_boarding_text_title,
                    ),
                  ),
                ),


                Visibility(
                  visible: _currentPage == 2,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: AppColors.on_boarding_text_next,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.circular(SizeConfig.scaleWidth(60))),
                      minimumSize: Size(
                        SizeConfig.scaleWidth(141),
                        SizeConfig.scaleHeight(45),
                      ),
                    ),
                    onPressed: () {
                      Get.offNamed('/sign_in_screen');
                    },
                    child: StoreText(
                      text: 'Get started',
                      textColor: Colors.white,
                      fontSize: SizeConfig.scaleTextFont(15),
                    ),
                  ),
                ),


                Visibility(
                  visible: _currentPage != 2,
                  child: TextButton(
                    onPressed: () {
                      _pageController.nextPage(
                          duration: Duration(milliseconds: 100),
                          curve: Curves.easeIn);
                    },
                    child: StoreText(
                      text: 'NEXT',
                      textColor: AppColors.on_boarding_text_next,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Visibility(
            visible: _currentPage != 2,
            child: Positioned(
              bottom: SizeConfig.scaleHeight(60),
              right: 0,
              left: 0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  OnBoardingIndicator(selected: _currentPage == 0),
                  OnBoardingIndicator(selected: _currentPage == 1),
                  OnBoardingIndicator(selected: _currentPage == 2),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
