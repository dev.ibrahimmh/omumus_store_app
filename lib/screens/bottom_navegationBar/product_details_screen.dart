import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/getx_controller/cart_getx_controller.dart';
import 'package:omumus_store_app/getx_controller/product_details_getx_controller.dart';
import 'package:omumus_store_app/getx_controller/product_getx_controller.dart';
import 'package:omumus_store_app/models/cart_item.dart';
import 'package:omumus_store_app/models/product.dart';
import 'package:omumus_store_app/models/product_details.dart';
import 'package:omumus_store_app/screens/bottom_navegationBar/favorite_screen.dart';
import 'package:omumus_store_app/screens/bottom_navegationBar/main_screen.dart';
import 'package:omumus_store_app/storage/user_shared_pref.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/helpers.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/app_elevated_button.dart';
import 'package:omumus_store_app/widgets/store_text.dart';

class ProductDetailsScreen extends StatefulWidget {
  final Product product;
  ProductDetailsScreen({required this.product});

  @override
  _ProductDetailsScreenState createState() => _ProductDetailsScreenState();
}

class _ProductDetailsScreenState extends State<ProductDetailsScreen> {
  ProductDetailsGetxController controller = Get.put(ProductDetailsGetxController());
  CartGetxController controllerCart = Get.put(CartGetxController());

  int cardIncrement = 1;


  @override
  Widget build(BuildContext context) {
    controller.getProductDetails(widget.product.id);
        print("ID: ${widget.product.id}");
    final PageController controllerPV = PageController(initialPage: 0);

    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              onPressed: () {
                Get.to(MainScreen(
                  indexid: 1,
                ));
              },
              icon: Icon(Icons.favorite))
        ],
      ),
      body: GetX<ProductDetailsGetxController>(
        builder: (ProductDetailsGetxController controller) {
          return controller.productsDetails.isEmpty
              ? Center(child: CircularProgressIndicator())
              : Stack(
                  children: [
                    PageView(
                      scrollDirection: Axis.horizontal,
                      controller: controllerPV,
                      children: <Widget>[
                        ClipRRect(
                          child: CachedNetworkImage(
                              imageUrl: controller
                                  .productsDetails[0]!.images[0].imageUrl,
                              placeholder: (context, url) => Center(
                                    child: CircularProgressIndicator(
                                      color:
                                          AppColors.main_screen_selected_icon,
                                    ),
                                  ),
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error),
                              height: SizeConfig.scaleHeight(500),
                              width: double.infinity,
                              fit: BoxFit.cover),
                        ),
                        ClipRRect(
                          child: CachedNetworkImage(
                              imageUrl: controller
                                  .productsDetails[0]!.images[1].imageUrl,
                              placeholder: (context, url) => Center(
                                    child: CircularProgressIndicator(
                                      color:
                                          AppColors.main_screen_selected_icon,
                                    ),
                                  ),
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error),
                              height: SizeConfig.scaleHeight(500),
                              width: double.infinity,
                              fit: BoxFit.cover),
                        ),
                        ClipRRect(
                          child: CachedNetworkImage(
                              imageUrl: controller
                                  .productsDetails[0]!.images[2].imageUrl,
                              placeholder: (context, url) => Center(
                                    child: CircularProgressIndicator(
                                      color:
                                          AppColors.main_screen_selected_icon,
                                    ),
                                  ),
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error),
                              height: SizeConfig.scaleHeight(500),
                              width: double.infinity,
                              fit: BoxFit.cover),
                        ),
                      ],
                    ),
                    Align(
                      alignment: AlignmentDirectional.bottomCenter,
                      child: Container(
                        height: SizeConfig.scaleHeight(350),
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(40),
                            topRight: Radius.circular(40),
                          ),
                        ),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 24, vertical: 40),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: StoreText(
                                      text:
                                          controller.productsDetails[0]!.nameEn,
                                      textColor: Colors.black,
                                      fontSize: 26,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () async {
                                      await ProductGetxController.to
                                          .addFavoriteProducts(
                                        context: context,
                                        id: controller.productsDetails[0]!.id,
                                      );
                                      setState(() {});
                                    },
                                    child: Container(
                                      height: 52,
                                      width: 52,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color:   ProductGetxController.to.favoriteProducts
                                            .where(
                                                (element) => element.id == widget.product.id)
                                            .isNotEmpty
                                            ? Colors.red
                                            : Colors.grey,
                                      ),
                                      child: Icon(
                                        ProductGetxController.to.favoriteProducts
                                            .where(
                                                (element) => element.id == widget.product.id)
                                            .isNotEmpty
                                            ? Icons.favorite
                                            : Icons.favorite_border,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              StoreText(
                                text: '${controller.productsDetails[0]!.price} \$',
                                textColor: Colors.black,
                                fontSize: 26,
                                fontWeight: FontWeight.w600,
                                textAlign: TextAlign.start,
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              StoreText(
                                text: controller.productsDetails[0]!.infoEn,
                                textColor: Colors.grey,
                                fontSize: 12,
                                textAlign: TextAlign.start,
                              ),
                              RatingBar.builder(
                                initialRating: double.parse(
                                    controller.productsDetails[0]!.productRate.toString()),
                                minRating: 1,
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                itemPadding:
                                    EdgeInsets.symmetric(horizontal: 4.0),
                                itemSize: 50,
                                itemBuilder: (context, _) => Icon(
                                  Icons.star,
                                  color: Colors.amber,
                                ),
                                onRatingUpdate: (rating) {
                                  ProductGetxController.to.rattingProduct(
                                      productId: controller.productsDetails[0]!.id,
                                      context: context,
                                      rate: rating);
                                },
                              ),
                              Spacer(),
                              AppElevatedButton(
                                onPressed: () async {
                                  bool status = await CartGetxController.to
                                      .createCartItem(cartItem);
                                  if (status) {
                                    Helpers.showSnackBar(context: context, message: 'add suc');
                                    cardIncrement = 1;
                                  } else {
                                    Helpers.showSnackBar(context: context, message: 'add fal');
                                    cardIncrement = 1;
                                  }
                                  Get.back();
                                },
                                text: 'Add to cart',
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                );
        },
      ),
    );
  }



  CartItem get cartItem {
    CartItem cartItem = CartItem();
    cartItem.imageUrl = widget.product.imageUrl;
    cartItem.productId = widget.product.id;
    cartItem.price = widget.product.price;
    cartItem.nameEn = widget.product.nameEn;
    cartItem.nameAr = widget.product.nameAr;
    cartItem.quantity = cardIncrement;
    return cartItem;
  }

}




/*
* GetX<ProductGetxController>(
        builder: (ProductGetxController controller) {
          return controller.productsDetails.isEmpty
              ? Center(child: CircularProgressIndicator())
              : Stack(
                  children: [
                    CachedNetworkImage(
                        imageUrl: widget.product.imageUrl,
                        placeholder: (context, url) =>
                            Center(
                              child: CircularProgressIndicator(
                                color: AppColors.main_screen_selected_icon,
                              ),
                            ),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                        height: SizeConfig.scaleHeight(500),
                        width: double.infinity,
                        fit: BoxFit.cover),
                    Align(
                      alignment: AlignmentDirectional.bottomCenter,
                      child: Container(
                        height: SizeConfig.scaleHeight(350),
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(40),
                            topRight: Radius.circular(40),
                          ),
                        ),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 24, vertical: 40),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: StoreText(
                                      text: widget.product.nameEn,
                                      textColor: Colors.black,
                                      fontSize: 26,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () async {
                                      await ProductGetxController.to
                                          .addFavoriteProducts(
                                              context: context,
                                              product: widget.product);
                                      setState(() {});
                                    },
                                    child: Container(
                                      height: 52,
                                      width: 52,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: widget.product.isFavorite
                                            ? Colors.red
                                            : Colors.grey,
                                      ),
                                      child: Icon(
                                        Icons.favorite,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              StoreText(
                                text: '${widget.product.price} \$',
                                textColor: Colors.black,
                                fontSize: 26,
                                fontWeight: FontWeight.w600,
                                textAlign: TextAlign.start,
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              StoreText(
                                text: widget.product.infoEn,
                                textColor: Colors.grey,
                                fontSize: 12,
                                textAlign: TextAlign.start,
                              ),
                              RatingBar.builder(
                                initialRating: double.parse(
                                    widget.product.productRate.toString()),
                                minRating: 1,
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                itemPadding:
                                    EdgeInsets.symmetric(horizontal: 4.0),
                                itemSize: 50,
                                itemBuilder: (context, _) => Icon(
                                  Icons.star,
                                  color: Colors.amber,
                                ),
                                onRatingUpdate: (rating) {
                                  ProductGetxController.to.rattingProduct(
                                      product: widget.product,
                                      context: context,
                                      rate: rating);
                                },
                              ),
                              Spacer(),
                              AppElevatedButton(
                                onPressed: () async {},
                                text: 'Add to cart',
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                );
        },
      ),
* */

/*
*  PageView(
                      scrollDirection: Axis.horizontal,
                      controller: controller,
                      children: <Widget>[
                        ClipRRect(
                          child: CachedNetworkImage(
                              imageUrl: controllerPGX.productsDetails,
                              placeholder: (context, url) =>
                                  Center(
                                    child: CircularProgressIndicator(
                                      color: AppColors.main_screen_selected_icon,
                                    ),
                                  ),
                              errorWidget: (context, url, error) => Icon(Icons.error),
                              height: SizeConfig.scaleHeight(500),
                              width: double.infinity,
                              fit: BoxFit.cover),
                        ),
                        ClipRRect(
                            child: Image.network(
                          ProductGX.detailedProduct.value!.images[0].imageUrl,
                          fit: BoxFit.fill,
                          width: SizeConfig.scaleWidth(280),
                          height: SizeConfig.scaleHeight(280),
                        )),
                        ClipRRect(
                            child: Image.network(
                          ProductGX.detailedProduct.value!.images[0].imageUrl,
                          fit: BoxFit.fill,
                          width: SizeConfig.scaleWidth(280),
                          height: SizeConfig.scaleHeight(280),
                        )),
                      ],
                    ),
* */

/*
*  CachedNetworkImage(
                  imageUrl: ,
                  placeholder: (context, url) =>
                      Center(
                        child: CircularProgressIndicator(
                          color: AppColors.main_screen_selected_icon,
                        ),
                      ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                  height: SizeConfig.scaleHeight(500),
                  width: double.infinity,
                  fit: BoxFit.cover),
* */

/*
* Stack(
        children: [
          CachedNetworkImage(
              imageUrl: ,
              placeholder: (context, url) => Center(
                    child: CircularProgressIndicator(
                      color: AppColors.main_screen_selected_icon,
                    ),
                  ),
              errorWidget: (context, url, error) => Icon(Icons.error),
              height: SizeConfig.scaleHeight(500),
              width: double.infinity,
              fit: BoxFit.cover),
          Align(
            alignment: AlignmentDirectional.bottomCenter,
            child: Container(
              height: SizeConfig.scaleHeight(350),
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 24, vertical: 40),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: StoreText(
                            text: widget.product.nameEn,
                            textColor: Colors.black,
                            fontSize: 26,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        GestureDetector(
                          onTap: () async {
                            await ProductGetxController.to.addFavoriteProducts(
                                context: context, product: widget.product);
                            setState(() {});
                          },
                          child: Container(
                            height: 52,
                            width: 52,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: widget.product.isFavorite
                                  ? Colors.red
                                  : Colors.grey,
                            ),
                            child: Icon(
                              Icons.favorite,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                    StoreText(
                      text: '${widget.product.price} \$',
                      textColor: Colors.black,
                      fontSize: 26,
                      fontWeight: FontWeight.w600,
                      textAlign: TextAlign.start,
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    StoreText(
                      text: widget.product.infoEn,
                      textColor: Colors.grey,
                      fontSize: 12,
                      textAlign: TextAlign.start,
                    ),
                    RatingBar.builder(
                      initialRating:
                          double.parse(widget.product.productRate.toString()),
                      minRating: 1,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                      itemSize: 50,
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (rating) {
                        ProductGetxController.to.rattingProduct(
                            product: widget.product,
                            context: context,
                            rate: rating);
                      },
                    ),
                    Spacer(),
                    AppElevatedButton(
                      onPressed: () async {},
                      text: 'Add to cart',
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
* */

/*
*
* Stack(
        children: [
          CachedNetworkImage(
              imageUrl: widget.product.imageUrl,
              placeholder: (context, url) =>
                  Center(
                    child: CircularProgressIndicator(
                      color: AppColors.main_screen_selected_icon,
                    ),
                  ),
              errorWidget: (context, url, error) => Icon(Icons.error),
              height: SizeConfig.scaleHeight(500),
              width: double.infinity,
              fit: BoxFit.cover),
          Align(
            alignment: AlignmentDirectional.bottomCenter,
            child: Container(
              height: SizeConfig.scaleHeight(350),
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: 24, vertical: 40),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: StoreText(
                            text: widget.product.nameEn,
                            textColor: Colors.black,
                            fontSize: 26,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        GestureDetector(
                          onTap: () async {
                            await ProductGetxController.to
                                .addFavoriteProducts(
                                context: context,
                                product: widget.product);
                            setState(() {});
                          },
                          child: Container(
                            height: 52,
                            width: 52,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: widget.product.isFavorite
                                  ? Colors.red
                                  : Colors.grey,
                            ),
                            child: Icon(
                              Icons.favorite,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                    StoreText(
                      text: '${widget.product.price} \$',
                      textColor: Colors.black,
                      fontSize: 26,
                      fontWeight: FontWeight.w600,
                      textAlign: TextAlign.start,
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    StoreText(
                      text: widget.product.infoEn,
                      textColor: Colors.grey,
                      fontSize: 12,
                      textAlign: TextAlign.start,
                    ),
                    RatingBar.builder(
                      initialRating: double.parse(
                          widget.product.productRate.toString()),
                      minRating: 1,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemPadding:
                      EdgeInsets.symmetric(horizontal: 4.0),
                      itemSize: 50,
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (rating) {
                        ProductGetxController.to.rattingProduct(
                            product: widget.product,
                            context: context,
                            rate: rating);
                      },
                    ),
                    Spacer(),
                    AppElevatedButton(
                      onPressed: () async {},
                      text: 'Add to cart',
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
* */
