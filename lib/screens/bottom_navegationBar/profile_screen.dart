import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/screens/bottom_navegationBar/profile_contant/addresses/address_screen.dart';
import 'package:omumus_store_app/screens/bottom_navegationBar/profile_contant/cards/cards_screen.dart';
import 'package:omumus_store_app/storage/user_shared_pref.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/helpers.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/list_tile_profile.dart';
import 'package:omumus_store_app/widgets/store_text.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CircleAvatar(
          radius: 70,
          backgroundImage: AssetImage('images/1257.jpg'),
        ),
        SizedBox(height: SizeConfig.scaleHeight(25)),
        StoreText(
          text: UserSharedPref().getName(),
          fontWeight: FontWeight.w600,
          textColor: AppColors.main_screen_selected_icon,
          textAlign: TextAlign.center,
          fontSize: 20,
        ),
        SizedBox(height: SizeConfig.scaleHeight(35)),
        Expanded(
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30),
            ),
            color: AppColors.main_screen_textField,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  ListTileProfile(
                    title: 'Language',
                    leading: Icon(
                      Icons.language,
                      color: AppColors.main_screen_selected_icon,
                    ),
                    trailing: Icon(
                      Icons.arrow_forward_ios,
                      color: AppColors.main_screen_selected_icon,
                      size: SizeConfig.scaleWidth(20),
                    ),
                  ),
                  Divider(
                    thickness: 1,
                    indent: SizeConfig.scaleWidth(28),
                    endIndent: SizeConfig.scaleWidth(28),
                  ),
                  ListTileProfile(
                    title: 'Profile',
                    leading: Icon(
                      Icons.person_outline,
                      color: AppColors.main_screen_selected_icon,
                    ),
                    trailing: Icon(
                      Icons.arrow_forward_ios,
                      color: AppColors.main_screen_selected_icon,
                      size: SizeConfig.scaleWidth(20),
                    ),
                  ),
                  Divider(
                    thickness: 1,
                    indent: SizeConfig.scaleWidth(28),
                    endIndent: SizeConfig.scaleWidth(28),
                  ),
                  ListTileProfile(
                    title: 'Change Password',
                    leading: Icon(
                      Icons.password,
                      color: AppColors.main_screen_selected_icon,
                    ),
                    trailing: Icon(
                      Icons.arrow_forward_ios,
                      color: AppColors.main_screen_selected_icon,
                      size: SizeConfig.scaleWidth(20),
                    ),
                  ),
                  Divider(
                    thickness: 1,
                    indent: SizeConfig.scaleWidth(28),
                    endIndent: SizeConfig.scaleWidth(28),
                  ),
                  ListTileProfile(
                    onTap: (){
                      Get.to(AddressesScreen());
                    },
                    title: 'Addresses',
                    leading: Icon(
                      Icons.location_on,
                      color: AppColors.main_screen_selected_icon,
                    ),
                    trailing: Icon(
                      Icons.arrow_forward_ios,
                      color: AppColors.main_screen_selected_icon,
                      size: SizeConfig.scaleWidth(20),
                    ),
                  ),
                  Divider(
                    thickness: 1,
                    indent: SizeConfig.scaleWidth(28),
                    endIndent: SizeConfig.scaleWidth(28),
                  ),
                  ListTileProfile(
                    title: 'Management Cards',
                    leading: Icon(
                      Icons.payment_rounded,
                      color: AppColors.main_screen_selected_icon,
                    ),
                    trailing: Icon(
                      Icons.arrow_forward_ios,
                      color: AppColors.main_screen_selected_icon,
                      size: SizeConfig.scaleWidth(20),
                    ),
                    onTap: (){
                      Get.to(CardsScreen());
                    },
                  ),
                  Divider(
                    thickness: 1,
                    indent: SizeConfig.scaleWidth(28),
                    endIndent: SizeConfig.scaleWidth(28),
                  ),
                  ListTileProfile(
                    title: 'Contact Us',
                    leading: Icon(
                      Icons.call,
                      color: AppColors.main_screen_selected_icon,
                    ),
                    trailing: Icon(
                      Icons.arrow_forward_ios,
                      color: AppColors.main_screen_selected_icon,
                      size: SizeConfig.scaleWidth(20),
                    ),
                  ),
                  Divider(
                    thickness: 1,
                    indent: SizeConfig.scaleWidth(28),
                    endIndent: SizeConfig.scaleWidth(28),
                  ),
                  ListTileProfile(
                    title: 'About Us',
                    leading: Icon(
                      Icons.info,
                      color: AppColors.main_screen_selected_icon,
                    ),
                    trailing: Icon(
                      Icons.arrow_forward_ios,
                      color: AppColors.main_screen_selected_icon,
                      size: SizeConfig.scaleWidth(20),
                    ),
                  ),
                  Divider(
                    thickness: 1,
                    indent: SizeConfig.scaleWidth(28),
                    endIndent: SizeConfig.scaleWidth(28),
                  ),
                  ListTileProfile(
                    onTap: ()async{
                      await logout();
                    },
                    title: 'Logout',
                    leading: Icon(
                      Icons.logout,
                      color: AppColors.main_screen_selected_icon,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  Future logout() async{
    bool loggedOut = await UserSharedPref().logout();
    if(loggedOut){
      Get.offAndToNamed('/sign_in_screen');
    }else{
      Helpers.showSnackBar(context: context, message: 'Logout failed', error: true);
    }
  }
}
