import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/getx_controller/categories_getx_controller.dart';
import 'package:omumus_store_app/models/sub_category.dart';
import 'package:omumus_store_app/screens/bottom_navegationBar/product_screen.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/card_categories.dart';
import 'package:omumus_store_app/widgets/store_text.dart';

class SubCategoryScreen extends StatefulWidget {
  final int id;

  SubCategoryScreen({required this.id});

  @override
  _SubCategoryScreenState createState() => _SubCategoryScreenState();
}

class _SubCategoryScreenState extends State<SubCategoryScreen> {
  CategoryGetxController controller = Get.put(CategoryGetxController());

  @override
  void initState() {
    // TODO: implement initState
    controller.getSubCategories(id: widget.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        toolbarHeight: 100,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: StoreText(
          text: 'Sub Category',
          fontSize: 20,
          fontWeight: FontWeight.w600,
          textAlign: TextAlign.center,
          textColor: AppColors.sign_in_text_no_account,
        ),
        iconTheme: IconThemeData(
          color: AppColors.sign_in_text_no_account,
        ),
      ),
      body: GetX<CategoryGetxController>(
        builder: (CategoryGetxController controller) {
          return controller.subCategories.isEmpty
              ? Center(child: CircularProgressIndicator())
              : Padding(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(30)),
            child: ListView.builder(
              itemCount: controller.subCategories.length,
              itemBuilder: (context, index) {
                SubCategory subCategory = controller.subCategories[index];
                return CardCategories(
                  categoryTitle: subCategory.nameEn,
                  imagePath: subCategory.imageUrl,
                  goSubCategory: (){
                    Get.to(ProductScreen(
                        id: controller.subCategories[index].id));
                  },
                );
              },
            ),
          );
        },
      ),
    );
  }
}

/**
 *Padding(
    padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(30)),
    child: ListView.builder(
    itemCount: 2,
    itemBuilder: (context, index) {
    return CardCategories(
    categoryTitle: 'Wedding Allowance',
    imagePath: 'images/pngwing2.png',
    goSubCategory: (){
    Get.toNamed('/product_screen');
    },
    );
    },
    ),
    ),
 */
