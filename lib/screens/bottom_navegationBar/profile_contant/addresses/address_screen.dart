import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:omumus_store_app/getx_controller/address_getx_controller.dart';
import 'package:omumus_store_app/models/address.dart';
import 'package:omumus_store_app/screens/bottom_navegationBar/profile_contant/addresses/add_address.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/helpers.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/list_tile_profile.dart';
import 'package:omumus_store_app/widgets/store_text.dart';

class AddressesScreen extends StatefulWidget {
  const AddressesScreen({Key? key}) : super(key: key);

  @override
  _AddressesScreenState createState() => _AddressesScreenState();
}

class _AddressesScreenState extends State<AddressesScreen> {
  AddressGetxController controller = Get.put(AddressGetxController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: StoreText(
          text: 'Address',
          textAlign: TextAlign.center,
          fontSize: 20,
          fontWeight: FontWeight.w600,
          textColor: AppColors.main_screen_selected_icon,
        ),
        iconTheme: IconThemeData(color: AppColors.main_screen_selected_icon),
      ),
      body: GetX<AddressGetxController>(
        builder: (AddressGetxController controller) {
          return Stack(
                  children: [
                    ListView.builder(
                      itemCount: controller.addresses.length,
                      itemBuilder: (context, index) {
                        Address address = controller.addresses[index];
                        return ListTileProfile(
                          title: address.name,
                          subTitle: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(height: SizeConfig.scaleHeight(5)),
                              StoreText(
                                text: address.info,
                                textColor: AppColors.on_boarding_text_details,
                                fontSize: 12,
                              ),
                              SizedBox(height: SizeConfig.scaleHeight(5)),
                              StoreText(
                                text: '970${address.contactNumber}',
                                textColor: AppColors.on_boarding_text_details,
                                fontSize: 15,
                              ),
                            ],
                          ),
                          leading: Icon(
                            Icons.location_on,
                            color: AppColors.main_screen_selected_icon,
                          ),
                          trailing: Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              IconButton(
                                onPressed: () {},
                                icon: Icon(
                                  Icons.edit,
                                  color: AppColors.main_screen_selected_icon,
                                ),
                              ),
                              IconButton(
                                onPressed: () async{
                                  await delete(address.id);
                                },
                                icon: Icon(
                                  Icons.delete,
                                  color: Colors.red,
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                    Positioned(
                      bottom: SizeConfig.scaleHeight(30),
                      right: SizeConfig.scaleWidth(30),
                      child: SizedBox(
                        height: SizeConfig.scaleHeight(70),
                        width: SizeConfig.scaleWidth(70),
                        child: FloatingActionButton(
                          onPressed: () {
                            Get.to(AddAddress());
                          },
                          elevation: 4,
                          backgroundColor: AppColors.BACKGROUND_SPLASH,
                          child: Icon(
                            Icons.add,
                            size: 40,
                          ),
                        ),
                      ),
                    ),
                  ],
                );
        },
      ),
    );
  }

  Future delete(int id) async{
    bool deleted = await AddressGetxController.to.deleteAddress(context, id: id);
    String message = deleted? 'Address deleted successfully' : 'Failed to delete address';
    Helpers.showSnackBar(context: context, message: message, error: !deleted);
  }


}






/*
* Stack(
        children: [
          ListView.builder(
            itemCount: 3,
            itemBuilder: (context, index) {
              return ListTileProfile(
                title: 'title',
                leading: Icon(
                  Icons.location_on,
                  color: AppColors.main_screen_selected_icon,
                ),
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    IconButton(
                      onPressed: () {},
                      icon: Icon(
                        Icons.edit,
                        color: AppColors.main_screen_selected_icon,
                      ),
                    ),
                    IconButton(
                      onPressed: () {},
                      icon: Icon(
                        Icons.delete,
                        color: Colors.red,
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
          Positioned(
            bottom: SizeConfig.scaleHeight(30),
            right: SizeConfig.scaleWidth(30),
            child: SizedBox(
              height: SizeConfig.scaleHeight(80),
              width: SizeConfig.scaleWidth(80),
              child: FloatingActionButton(
                onPressed: () {},
                elevation: 4,
                backgroundColor: AppColors.BACKGROUND_SPLASH,
                child: Icon(
                  Icons.add,
                  size: 40,
                ),
              ),
            ),
          ),
        ],
      ),
* */
