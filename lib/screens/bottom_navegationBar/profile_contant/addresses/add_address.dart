import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/api/controllers/address_api_controller.dart';
import 'package:omumus_store_app/getx_controller/address_getx_controller.dart';
import 'package:omumus_store_app/getx_controller/city_getx_controller.dart';
import 'package:omumus_store_app/models/address.dart';
import 'package:omumus_store_app/models/city.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/helpers.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/store_text.dart';
import 'package:omumus_store_app/widgets/store_text_field.dart';

class AddAddress extends StatefulWidget {
  const AddAddress({Key? key}) : super(key: key);

  @override
  _AddAddressState createState() => _AddAddressState();
}

class _AddAddressState extends State<AddAddress> {
  late TextEditingController _nameTextEditingController;
  late TextEditingController _mobileTextEditingController;
  late TextEditingController _addressDescTextEditingController;

  int cityId = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameTextEditingController = TextEditingController();
    _mobileTextEditingController = TextEditingController();
    _addressDescTextEditingController = TextEditingController();
  }


  @override
  void dispose() {
    // TODO: implement dispose
    _nameTextEditingController.dispose();
    _mobileTextEditingController.dispose();
    _addressDescTextEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: StoreText(
          text: 'Add Address',
          textAlign: TextAlign.center,
          fontSize: 20,
          fontWeight: FontWeight.w600,
          textColor: AppColors.main_screen_selected_icon,
        ),
        iconTheme: IconThemeData(color: AppColors.main_screen_selected_icon),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(30)),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: SizeConfig.scaleHeight(60)),
              StoreTextField(
                textEditingController: _nameTextEditingController,
                labelText: 'Address Name',
                hintText: 'Enter your address name',
                // keyboardTextInputType: TextInputType.phone,
                suffixIcon: Icon(
                  Icons.perm_identity,
                  color: AppColors.on_boarding_text_field_text,
                ),
              ),
              SizedBox(height: SizeConfig.scaleHeight(30)),
              StoreTextField(
                textEditingController: _addressDescTextEditingController,
                labelText: 'Address description',
                hintText: 'Enter your street and building name',
                // keyboardTextInputType: TextInputType.phone,
                suffixIcon: Icon(
                  Icons.location_on,
                  color: AppColors.on_boarding_text_field_text,
                ),
              ),
              SizedBox(height: SizeConfig.scaleHeight(30)),
              StoreTextField(
                textEditingController: _mobileTextEditingController,
                labelText: 'Phone number',
                hintText: 'Enter your phone number',
                keyboardTextInputType: TextInputType.phone,
                prefixText: '+970| ',
                suffixIcon: Icon(
                  Icons.phone,
                  color: AppColors.on_boarding_text_field_text,
                ),
              ),
              SizedBox(height: SizeConfig.scaleHeight(30)),
              Container(
                padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.scaleWidth(15),
                    vertical: SizeConfig.scaleHeight(5)),
                decoration: BoxDecoration(
                  color:
                  AppColors.on_boarding_text_field_text.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(60),
                ),
                child:  GetX<CityGetxController>(
                  // init: CityGetxController(),
                    builder : (CityGetxController controller){
                      if(controller.city.isNotEmpty){
                        print(controller.city.length);
                        return DropdownButtonFormField<City>(
                          items: controller.city.value.map((e) => DropdownMenuItem<City>(
                            child: Text(e.nameEn.toString()),
                            // value: e.nameEn,
                            value: e,
                          )).toList(),
                          value: controller.city.value.elementAt(0),
                          onChanged: (City? city){
                            setState(() {
                              cityId = city!.id;
                            });
                          },
                        );

                      }else{
                        print(controller.city.length);

                        return Text("");
                      }
                    }),
              ),
              SizedBox(height: SizeConfig.scaleHeight(45)),
              ElevatedButton(
                onPressed: () async {
                  await performSave();
                },
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(
                    double.infinity,
                    SizeConfig.scaleHeight(50),
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadiusDirectional.circular(70),
                  ),
                  primary: AppColors.sign_in_btn,
                ),
                child: StoreText(
                  text: 'Add Address',
                  fontWeight: FontWeight.w500,
                  textColor: Colors.white,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future performSave() async {
    if(checkData()){
      await save();
    }
  }

  bool checkData() {
    if (_nameTextEditingController.text.isNotEmpty &&
        _addressDescTextEditingController.text.isNotEmpty &&
        _mobileTextEditingController.text.isNotEmpty &&
        cityId != 0) {
      return true;
    }
    return false;
  }

  Future save() async {
    bool state = await AddressGetxController.to.addAddress(
      context,
      addressName: _nameTextEditingController.text,
      addressInfo: _addressDescTextEditingController.text,
      contactNumber: _mobileTextEditingController.text,
      cityId: cityId,
    );
    String message = state ? 'Add Successfully' : 'Failed to add';
    Helpers.showSnackBar(context: context, message: message,error: !state);
  }
}
