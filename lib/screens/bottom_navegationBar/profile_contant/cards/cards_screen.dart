import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/getx_controller/card_payment_getx_controller.dart';
import 'package:omumus_store_app/models/card_payment.dart';
import 'package:omumus_store_app/screens/bottom_navegationBar/profile_contant/cards/add_credit_card_screen.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/helpers.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/card_payment_widget.dart';
import 'package:omumus_store_app/widgets/list_tile_profile.dart';
import 'package:omumus_store_app/widgets/store_text.dart';

class CardsScreen extends StatefulWidget {
  const CardsScreen({Key? key}) : super(key: key);

  @override
  _CardsScreenState createState() => _CardsScreenState();
}

class _CardsScreenState extends State<CardsScreen> {
  CardPaymentGetxController controller = Get.put(CardPaymentGetxController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: StoreText(
          text: 'Card Payment',
          textAlign: TextAlign.center,
          fontSize: 20,
          fontWeight: FontWeight.w600,
          textColor: AppColors.main_screen_selected_icon,
        ),
        iconTheme: IconThemeData(color: AppColors.main_screen_selected_icon),
      ),
      body: GetX<CardPaymentGetxController>(
        builder: (CardPaymentGetxController controller) {
          return Stack(
            children: [
              ListView.builder(
                itemCount: controller.paymentCards.length,
                itemBuilder: (context, index) {
                  CardPayment cardPayment = controller.paymentCards[index];
                  return MyCardWidget(
                    cardHolderName: cardPayment.holderName,
                    cardNumber: cardPayment.cardNumber,
                    cvvCode: cardPayment.cvv.toString(),
                    expiryDate: cardPayment.expDate,
                    cardType: CardType.visa,
                    onTapDelete: () async {
                      await delete(cardPayment.id);
                    },
                  );
                },
              ),
              Positioned(
                bottom: SizeConfig.scaleHeight(30),
                right: SizeConfig.scaleWidth(30),
                child: SizedBox(
                  height: SizeConfig.scaleHeight(70),
                  width: SizeConfig.scaleWidth(70),
                  child: FloatingActionButton(
                    onPressed: () {
                      Get.to(AddCreditCardScreen());
                    },
                    elevation: 4,
                    backgroundColor: AppColors.BACKGROUND_SPLASH,
                    child: Icon(
                      Icons.add,
                      size: 40,
                    ),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }

  Future delete(int id) async{
    bool deleted = await CardPaymentGetxController.to.deleteCardPayment(context, id: id);
    String message = deleted? 'Card deleted successfully' : 'Failed to delete Card';
    Helpers.showSnackBar(context: context, message: message, error: !deleted);
  }
}


/*
* ListTileProfile(
                    title: cardPayment.holderName,
                    subTitle: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: SizeConfig.scaleHeight(5)),
                        StoreText(
                          text: cardPayment.cardNumber,
                          textColor: AppColors.on_boarding_text_details,
                          fontSize: 12,
                        ),
                        SizedBox(height: SizeConfig.scaleHeight(5)),
                        StoreText(
                          text: cardPayment.expDate,
                          textColor: AppColors.on_boarding_text_details,
                          fontSize: 15,
                        ),
                      ],
                    ),
                    leading: Icon(
                      Icons.location_on,
                      color: AppColors.main_screen_selected_icon,
                    ),
                    trailing: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.edit,
                            color: AppColors.main_screen_selected_icon,
                          ),
                        ),
                        IconButton(
                          onPressed: () async{
                            await delete(cardPayment.id);
                          },
                          icon: Icon(
                            Icons.delete,
                            color: Colors.red,
                          ),
                        ),
                      ],
                    ),
                  );
* */