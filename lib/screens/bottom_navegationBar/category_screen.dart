import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/api/controllers/category_api_controller.dart';
import 'package:omumus_store_app/getx_controller/categories_getx_controller.dart';
import 'package:omumus_store_app/models/category.dart';
import 'package:omumus_store_app/screens/bottom_navegationBar/sub_category_screen.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/card_categories.dart';
import 'package:omumus_store_app/widgets/store_text.dart';

class CategoryScreen extends StatefulWidget {
  const CategoryScreen({Key? key}) : super(key: key);

  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  CategoryGetxController controller = Get.put(CategoryGetxController());
  // late Future<List<Category>> _categoryFuture;
  // List<Category> _categories = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(30)),
      child: GetX<CategoryGetxController>(
        builder: (CategoryGetxController controller) {
          return controller.categories.isEmpty
              ? Center(child: CircularProgressIndicator())
              : ListView.builder(
            itemCount: controller.categories.length,
            itemBuilder: (context, index) {
              Category category = controller.categories[index];
              return CardCategories(
                categoryTitle: category.nameEn,
                imagePath: category.imageUrl,
                goSubCategory: () {
                  Get.to(SubCategoryScreen(
                      id: controller.categories[index].id));
                },
              );
            },
          );
        },
      ),
    );
  }
}

/*
ListView.builder(
              itemCount: _categories.length,
              itemBuilder: (context, index) {
                Category category = _categories.elementAt(index);
                print('categoryImage: ${category.imageUrl}');
                return CardCategories(
                  categoryTitle: category.nameEn,
                  imagePath: category.imageUrl,
                  goSubCategory: () {
                    Get.toNamed('/sub_category_screen');
                  },
                );
              },
            );
* */

/*
*
* Padding(
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(30)),
      child: FutureBuilder<List<Category>>(
        future: _categoryFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasData) {
            _categories = snapshot.data ?? [];
            print('categories ${_categories.length}');
            return ListView.builder(
              itemCount: _categories.length,
              itemBuilder: (context, index) {
                Category category = _categories.elementAt(index);
                print('categoryImage: ${category.imageUrl}');
                return CardCategories(
                  categoryTitle: category.nameEn,
                  imagePath: category.imageUrl,
                  goSubCategory: () {
                    Get.toNamed('/sub_category_screen');
                  },
                );
              },
            );
          } else if (snapshot.hasError) {
            return Center(
              child: Column(
                children: [
                  Icon(Icons.warning, size: 70),
                  StoreText(
                    text: 'NO DATA Erorr',
                     textColor: Colors.black,
                    fontSize: 20,
                  ),
                ],
              ),
            );
          }else{
            return Center(
              child: Column(
                children: [
                  Icon(Icons.warning, size: 70),
                  StoreText(
                    text: 'NO DATA',
                    textColor: Colors.black,
                    fontSize: 20,
                  ),
                ],
              ),
            );
          }
        },
      ),
    );
* */
