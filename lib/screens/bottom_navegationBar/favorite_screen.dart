import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/getx_controller/product_getx_controller.dart';
import 'package:omumus_store_app/models/product.dart';
import 'package:omumus_store_app/screens/bottom_navegationBar/product_details_screen.dart';
import 'package:omumus_store_app/widgets/product_card.dart';

class FavoriteScreen extends StatefulWidget {


  @override
  _FavoriteScreenState createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  ProductGetxController controller = Get.put(ProductGetxController());

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Obx((){
        return GridView.builder(
          // scrollDirection: Axis.vertical,
          padding: EdgeInsets.all(15),
          itemCount: ProductGetxController.to.favoriteProducts.length,
          shrinkWrap: true,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 10,
            childAspectRatio: 160 / 320,
            mainAxisSpacing: 10,
          ),
          itemBuilder: (context, index) {
            // Product product = controller.products[index];
            return ProductCard(
                product: ProductGetxController.to.favoriteProducts[index],
                onTap: (){
                  Get.to(ProductDetailsScreen(
                      product: controller.products[index]));
                });
          },
        );
      })
    );
  }
}


/*
* GridView.builder(
        scrollDirection: Axis.vertical,
        itemCount: ProductGetxController.to.favoriteProducts.length,
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 25,
          childAspectRatio: 146 / 215,
          mainAxisSpacing: 40,
        ),
        itemBuilder: (context, index) {
          // Product product = controller.products[index];
          return ProductCard(
              product: ProductGetxController.to.favoriteProducts[index],
              onTap: (){
                // Get.to(ProductDetailsScreen(
                //     product: product));
              });
        },
      ),
* */