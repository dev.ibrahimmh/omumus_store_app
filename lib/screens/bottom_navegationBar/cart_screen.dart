import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/getx_controller/cart_getx_controller.dart';
import 'package:omumus_store_app/models/cart_item.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/helpers.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/app_elevated_button.dart';
import 'package:omumus_store_app/widgets/product_cart_widget.dart';
import 'package:omumus_store_app/widgets/store_text.dart';

class CartScreen extends StatefulWidget {


  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {



  @override
  Widget build(BuildContext context) {
    return Container(
      child: GetX<CartGetxController>(
        builder: (CartGetxController controller) {
          return controller.loading.value
              ? Center(child: CircularProgressIndicator())
              : controller.cartItem.isNotEmpty
              ? Container(
            padding: EdgeInsets.symmetric(horizontal: 29, vertical: 10),
            child: Column(
              children: [
                Expanded(
                  child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    itemCount: controller.cartItem.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return ProductCartWidget(
                        cartItem: controller.cartItem[index],
                        onTap: () async {
                          await deleteItem(
                            cartItem: controller.cartItem[index],
                            context: context,
                          );
                        },
                      );
                    },
                  ),
                ),
                AppElevatedButton(
                  text: 'Make Order',
                  onPressed: () {
                    // Get.to(OrderScreen(cart: cart,));
                  },
                ),
              ],
            ),
          )
              : Center(
            child: Text('No Data'),
          );
        },
      ),
    );
  }

  deleteItem({required CartItem cartItem, required BuildContext context}) async {
    bool status = await CartGetxController.to.deleteItem(cartItem.id);
    String msg = status ? 'delete success' : 'delete fail';
    Helpers.showSnackBar(context: context, message: msg);
  }

  String get cart {
    List<Map<String, dynamic>> items = [];
    CartGetxController.to.cartItem.map((element) => items.add({'product_id': element.productId, 'quantity': element.quantity})).toList();
    return jsonEncode(items);
  }

}
