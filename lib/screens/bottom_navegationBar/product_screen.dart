import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/getx_controller/product_getx_controller.dart';
import 'package:omumus_store_app/models/product.dart';
import 'package:omumus_store_app/screens/bottom_navegationBar/product_details_screen.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/product_card.dart';
import 'package:omumus_store_app/widgets/store_text.dart';

class ProductScreen extends StatefulWidget {
  final int id;

  ProductScreen({required this.id});

  @override
  _ProductScreenState createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
  ProductGetxController controller = Get.put(ProductGetxController());


  @override
  Widget build(BuildContext context) {
    controller.getProduct(id: widget.id);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        toolbarHeight: 100,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: StoreText(
          text: 'Products',
          fontSize: 20,
          fontWeight: FontWeight.w600,
          textAlign: TextAlign.center,
          textColor: AppColors.sign_in_text_no_account,
        ),
        iconTheme: IconThemeData(
          color: AppColors.sign_in_text_no_account,
        ),
      ),
      body: Obx(
        () {
          return controller.products.isEmpty
              ? Center(child: CircularProgressIndicator())
              : GridView.builder(
                  padding: EdgeInsets.all(15),
                  itemCount: controller.products.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 10,
                    childAspectRatio: 160 / 320,
                  ),
                  itemBuilder: (context, index) {
                    Product product = controller.products[index];
                    return ProductCard(
                      product: product,
                      onTap: () {
                        Get.to(ProductDetailsScreen(
                            product: controller.products[index]));
                      },
                    );
                    //ProductCard(product: product),
                  },
                );
        },
      ),
    );
  }
}

/*
* GridView.builder(
            padding: EdgeInsets.all(15),
            itemCount: controller.products.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 10,
              crossAxisSpacing: 10,
              childAspectRatio: 148 / 200,
            ),
            itemBuilder: (context, index) {
              Product product = controller.products[index];
              return Card(
                clipBehavior: Clip.antiAlias,
                color: Colors.white,
                elevation: 3,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Stack(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: CachedNetworkImage(
                            height: 172,
                            width: double.infinity,
                            imageUrl: product.imageUrl,
                            placeholder: (context, url) => Center(
                              child: CircularProgressIndicator(
                                color: AppColors.main_screen_selected_icon,
                              ),
                            ),
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error),
                            fit: BoxFit.cover,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              StoreText(
                                text: product.nameEn,
                                fontSize: 12,
                                textColor: Colors.grey,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              StoreText(
                                text: '${product.price}\$',
                                fontSize: 15,
                                textColor: AppColors.on_boarding_text_title,
                                fontWeight: FontWeight.bold,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              );
            },
          );
* */

/*

 padding: EdgeInsets.all(15),

        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          childAspectRatio: 148/200,
        ),
        children: [
          Card(
            color: Colors.white,
            elevation: 3,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            ),
          ),
          Card(
            color: AppColors.on_boarding_text_details,
            elevation: 3,
          ),
          Card(
            color: AppColors.on_boarding_text_details,
            elevation: 3,
          ),
          Card(
            color: AppColors.on_boarding_text_details,
            elevation: 3,
          ),
          Card(
            color: AppColors.on_boarding_text_details,
            elevation: 3,
          ),
          Card(
            color: AppColors.on_boarding_text_details,
            elevation: 3,
          ),
          Card(
            color: AppColors.on_boarding_text_details,
            elevation: 3,
          ),
          Card(
            color: AppColors.on_boarding_text_details,
            elevation: 3,
          ),
          Card(
            color: AppColors.on_boarding_text_details,
            elevation: 3,
          ),
        ],

* */
