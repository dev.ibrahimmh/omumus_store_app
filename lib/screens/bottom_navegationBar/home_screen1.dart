import 'package:carousel_slider/carousel_slider.dart';

//import 'package:favorite_button/favorite_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/api/controllers/home_controller.dart';
import 'package:omumus_store_app/getx_controller/product_getx_controller.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/app_elevated_button.dart';
import 'package:omumus_store_app/widgets/store_text.dart';

class HomeScreen1 extends StatefulWidget {
  @override
  _HomeScreen1State createState() => _HomeScreen1State();
}

class _HomeScreen1State extends State<HomeScreen1> {
  HomeController _controller = Get.put(HomeController());
  late Future<bool> homeIndex;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print('0000000000');
    homeIndex = _controller.indexHome();
    print('we are here');
  }

  final List<String> images = [
    'images/suit.png',
    'images/sports.png',
    'images/medical_glasses.png',
  ];

  @override
  Widget build(BuildContext context) {
    // for (int i = 0; i < _controller.HomeOpj.value!.latestProducts.length; i++) {
    //   imageList.insert(
    //       i, _controller.HomeOpj.value!.latestProducts[i].imageUrl);
    // }
    final orientation = MediaQuery.of(context).orientation;
    return Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: AlignmentDirectional.topStart,
            end: AlignmentDirectional.bottomEnd,
            colors: [
              Colors.white,
              Colors.white,
              Colors.white,
              Colors.white,
              Colors.white,
              Colors.white,
              Colors.white,
            ],
          ),
        ),
        child: FutureBuilder(
          future: Future.wait([
            homeIndex,
          ]),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
                // CircularPercentIndicator(
                //   radius: 130.0,
                //   animation: true,
                //   animationDuration: 1000,
                //   lineWidth: 15.0,
                //   percent: 0.4,
                //   center: new Image.asset(
                //     'images/Unhappy.png',
                //     width: 80,
                //   ),
                //   circularStrokeCap: CircularStrokeCap.butt,
                //   backgroundColor: AppColors.GColor,
                //   progressColor: AppColors.COLOR,
                // ),
              );
            } else if (snapshot.hasData) {
              return ListView(children: [
                SizedBox(height: SizeConfig.scaleHeight(10)),
                CarouselSlider(
                  options: CarouselOptions(
                    height: SizeConfig.scaleHeight(250),
                    enlargeCenterPage: true,
                    enableInfiniteScroll: false,
                    autoPlay: true,
                  ),
                  items: images
                      .map((e) => Container(
                            decoration: BoxDecoration(
                              color: AppColors.BACKGROUND_SPLASH,
                              borderRadius: BorderRadius.circular(20),
                            ),
                            // clipBehavior: Clip.antiAlias,
                            child: ClipRRect(
                              clipBehavior: Clip.antiAlias,
                              child: Stack(
                                fit: StackFit.expand,
                                children: <Widget>[
                                  Image.asset(
                                    e,
                                    width: double.infinity,
                                    height: double.infinity,
                                    // fit: BoxFit.fill,
                                  )
                                ],
                              ),
                            ),
                          ))
                      .toList(),
                ),
                SizedBox(height: SizeConfig.scaleHeight(30)),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    children: [
                      StoreText(
                          text: 'Latest Products',
                          fontSize: 21,
                          fontWeight: FontWeight.w600,
                          textColor: AppColors.on_boarding_text_title),
                      Spacer(),
                      ElevatedButton(
                        onPressed: () {},
                        style: ElevatedButton.styleFrom(
                          primary: AppColors.BACKGROUND_SPLASH,
                        ),
                        child: StoreText(
                          text: 'See All',
                          textColor: AppColors.on_boarding_text_title,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 271,
                  width: 500,
                  child: GridView.builder(
                    shrinkWrap: true,
                    primary: false,
                    scrollDirection: Axis.horizontal,
                    padding: EdgeInsets.symmetric(horizontal: 30),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 1,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10,
                      childAspectRatio: 240 / 120,
                    ),
                    itemCount: _controller.HomeOpj.value!.latestProducts.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                            color: Colors.transparent,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                child: Center(
                                  child: Image.network(
                                    _controller.HomeOpj.value!
                                        .latestProducts[index].imageUrl,
                                    // height: 150,
                                  ),
                                ),
                              ),
                              SizedBox(height: SizeConfig.scaleHeight(7)),
                              Expanded(
                                child: StoreText(
                                    text: _controller.HomeOpj.value!
                                        .latestProducts[index].nameEn,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w500,
                                    textColor:
                                        AppColors.on_boarding_text_title),
                              ),
                              SizedBox(height: SizeConfig.scaleHeight(7)),
                              Row(
                                children: [
                                  Icon(Icons.timer,
                                      color: AppColors.BACKGROUND_SPLASH),
                                  SizedBox(width: SizeConfig.scaleHeight(7)),
                                  StoreText(
                                      text: '30  Min',
                                      fontSize: 15,
                                      fontWeight: FontWeight.w400,
                                      textColor: AppColors.BACKGROUND_SPLASH),
                                ],
                              ),
                              // Row(
                              //   children: [
                              //     // StarButton(
                              //     //   iconSize: 35,
                              //     //   isStarred: false,
                              //     //   // iconDisabledColor: Colors.white,
                              //     //   valueChanged: (_isStarred) {
                              //     //     print('Is Starred : $_isStarred');
                              //     //   },
                              //     // ),
                              //     // SizedBox(width: SizeConfig.scaleHeight(7)),
                              //     // MyText(
                              //     //     text: '4.3',
                              //     //     fontSize: 15,
                              //     //     fontWeight: FontWeight.w600,
                              //     //     color: AppColors.Auth_COLOR),
                              //     // Spacer(),
                              //     // SizedBox(width: SizeConfig.scaleWidth(50),)
                              //     // Icon(Icons.favorite, color: Colors.red),
                              //     FavoriteButton(
                              //       iconSize: 35,
                              //       isFavorite: true,
                              //       // iconDisabledColor: Colors.white,
                              //       valueChanged: (_isFavorite) {
                              //         print('Is Favorite : $_isFavorite');
                              //       },
                              //     ),
                              //   ],
                              // )
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(10)),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    children: [
                      StoreText(
                          text: 'Most Sales Product',
                          fontSize: 21,
                          fontWeight: FontWeight.w600,
                          textColor: AppColors.on_boarding_text_title),
                      Spacer(),
                      ElevatedButton(
                        onPressed: () {},
                        style: ElevatedButton.styleFrom(
                          primary: AppColors.BACKGROUND_SPLASH,
                        ),
                        child: StoreText(
                          text: 'See All',
                          textColor: AppColors.on_boarding_text_title,
                        ),
                      ),
                    ],
                  ),
                ),
                GridView.builder(
                  shrinkWrap: true,
                  primary: false,
                  scrollDirection: Axis.vertical,
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 1,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    childAspectRatio: 150 / 200,
                  ),
                  itemCount: _controller.HomeOpj.value!.famousProducts.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: Colors.transparent,
                          width: 1,
                        ),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Column(
                          // crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Stack(
                              children: [
                                Image.network(
                                  _controller.HomeOpj.value!
                                      .famousProducts[index].imageUrl,
                                  height: SizeConfig.scaleHeight(350),
                                ),
                                Positioned(
                                  bottom: 20,
                                  left: 10,
                                  right: 10,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        height: 25,
                                        width: 70,
                                        decoration: BoxDecoration(
                                          color: AppColors.BACKGROUND_SPLASH,
                                          borderRadius:
                                              BorderRadius.circular(5),
                                        ),
                                        child: Center(
                                          child: StoreText(
                                            text:
                                                '${_controller.HomeOpj.value!.famousProducts[index].price} \$',
                                            fontSize: 15,
                                            fontWeight: FontWeight.w500,
                                            textColor: AppColors
                                                .on_boarding_text_title,
                                          ),
                                        ),
                                      ),
                                      // SizedBox(width: SizeConfig.scaleWidth(30)),
                                      Row(
                                        children: [
                                          RatingBar.builder(
                                            initialRating: double.parse(
                                              _controller.HomeOpj.value!
                                                  .famousProducts[index].overalRate
                                                  .toString(),
                                            ),
                                            minRating: 1,
                                            direction: Axis.horizontal,
                                            allowHalfRating: false,
                                            itemCount: 1,
                                            itemPadding: EdgeInsets.symmetric(
                                                horizontal: 4.0),
                                            itemSize: 35,
                                            itemBuilder: (context, _) => Icon(
                                              Icons.star,
                                              color: Colors.amber,
                                            ),
                                            onRatingUpdate: (rating) {
                                              // ProductGetxController.to.rattingProduct(
                                              //     // productId: controller.productsDetails[0]!.id,
                                              //     context: context,
                                              //     rate: rating);
                                            },
                                          ),
                                          StoreText(
                                            text: _controller.HomeOpj.value!.famousProducts[index].productRate.toString(),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: SizeConfig.scaleHeight(5)),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 12),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: StoreText(
                                        text: _controller.HomeOpj.value!
                                            .famousProducts[index].nameEn,
                                        fontSize: 17,
                                        fontWeight: FontWeight.w600,
                                        textColor:
                                            AppColors.on_boarding_text_title),
                                  ),
                                  // Spacer(),
                                  // StarButton(
                                  //   iconSize: 35,
                                  //   isStarred: false,
                                  //   // iconDisabledColor: Colors.white,
                                  //   valueChanged: (_isStarred) {
                                  //     print('Is Starred : $_isStarred');
                                  //   },
                                  // ),
                                  // SizedBox(width: SizeConfig.scaleHeight(5)),
                                  // MyText(
                                  //     text: '4.3',
                                  //     fontSize: 15,
                                  //     fontWeight: FontWeight.w600,
                                  //     color: AppColors.Auth_COLOR),
                                  // SizedBox(width: SizeConfig.scaleWidth(10)),
                                  // FavoriteButton(
                                  //   iconSize: 35,
                                  //   isFavorite: true,
                                  //   // iconDisabledColor: Colors.white,
                                  //   valueChanged: (_isFavorite) {
                                  //     print('Is Favorite : $_isFavorite');
                                  //   },
                                  // ),
                                ],
                              ),
                            ),
                            SizedBox(height: SizeConfig.scaleHeight(5)),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 12),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: StoreText(
                                        text: _controller.HomeOpj.value!
                                            .famousProducts[index].infoEn,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w600,
                                        textColor:
                                            AppColors.on_boarding_text_details),
                                  ),
                                  // StoreText(
                                  //     text: '10:00 AM',
                                  //     fontSize: 15.9,
                                  //     fontWeight: FontWeight.w600,
                                  //     textColor:
                                  //         AppColors.main_screen_selected_icon),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ]);
            } else if (snapshot.hasError) {
              print('ERROR: ${snapshot.error}');
              return Center(
                child: Column(
                  children: [
                    Icon(Icons.warning, size: 70),
                    SizedBox(height: 20),
                    Text(
                      'NO DATA',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                    )
                  ],
                ),
              );
            } else {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.warning, size: 70),
                    SizedBox(height: 10),
                    Text(
                      'NO DATA',
                      style: TextStyle(
                        color: Colors.grey.shade500,
                        fontSize: 20,
                      ),
                    )
                  ],
                ),
              );
            }
          },
        ));
  }
}
/*,*/
