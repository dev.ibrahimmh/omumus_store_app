import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/getx_controller/cart_getx_controller.dart';
import 'package:omumus_store_app/getx_controller/product_getx_controller.dart';
import 'package:omumus_store_app/models/bn_screen.dart';
import 'package:omumus_store_app/screens/bottom_navegationBar/cart_screen.dart';
import 'package:omumus_store_app/screens/bottom_navegationBar/category_screen.dart';
import 'package:omumus_store_app/screens/bottom_navegationBar/favorite_screen.dart';
import 'package:omumus_store_app/screens/bottom_navegationBar/home_screen1.dart';
import 'package:omumus_store_app/screens/bottom_navegationBar/profile_screen.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/store_text.dart';
import 'package:omumus_store_app/widgets/text_field_search.dart';

class MainScreen extends StatefulWidget {
  final int indexid;
  MainScreen({required this.indexid});

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  ProductGetxController controllerP = Get.put(ProductGetxController());
  CartGetxController controllerCart = Get.put(CartGetxController());


  int _currentIndex = 0;


  List<BottomNavigationScreen> _bnScreen = <BottomNavigationScreen>[
    BottomNavigationScreen(
      HomeScreen1(),
      TextFieldSearch(),
    ),
    BottomNavigationScreen(
      FavoriteScreen(),
      StoreText(
        text: 'Wishlist',
        fontSize: 20,
        fontWeight: FontWeight.w600,
        textAlign: TextAlign.center,
        textColor: AppColors.sign_in_text_no_account,

      ),
    ),
    BottomNavigationScreen(
      CartScreen(),
      StoreText(
        text: 'My Cart',
        fontSize: 20,
        fontWeight: FontWeight.w600,
        textAlign: TextAlign.center,
        textColor: AppColors.sign_in_text_no_account,

      ),
    ),
    BottomNavigationScreen(
      CategoryScreen(),
      StoreText(
        text: 'Categories',
        fontSize: 20,
        fontWeight: FontWeight.w600,
        textAlign: TextAlign.center,
        textColor: AppColors.sign_in_text_no_account,

      ),
    ),
    BottomNavigationScreen(
      ProfileScreen(),
      StoreText(
        text: 'Profile',
        fontSize: 20,
        fontWeight: FontWeight.w600,
        textAlign: TextAlign.center,
        textColor: AppColors.sign_in_text_no_account,
      ),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        toolbarHeight: 100,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: widget.indexid == 1 ? _bnScreen.elementAt(_currentIndex = 1).widget2 : _bnScreen.elementAt(_currentIndex).widget2,
      ),
      body: widget.indexid == 1 ? _bnScreen.elementAt(_currentIndex = 1).widget : _bnScreen.elementAt(_currentIndex).widget,
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(20), topLeft: Radius.circular(20)),
          boxShadow: [
            BoxShadow(
                color: AppColors.main_screen_selected_icon.withOpacity(0.5),
                spreadRadius: 0,
                blurRadius: 5),
          ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20)),
          child: BottomNavigationBar(
            type: BottomNavigationBarType.shifting,
            showSelectedLabels: false,
            showUnselectedLabels: false,
            backgroundColor: Colors.white10,
            elevation: 0,
            iconSize: 30,
            unselectedItemColor: AppColors.main_screen_un_selected_icon,
            selectedItemColor: AppColors.main_screen_selected_icon,
            currentIndex: widget.indexid == 1 ? _currentIndex = 1 : _currentIndex,
            onTap: (int selectedIndex) {
              setState(() {
                _currentIndex = selectedIndex;
              });
            },
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.home_outlined,
                ),
                label: 'home',
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.favorite_border,
                ),
                label: 'home',
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.shopping_cart_outlined,
                ),
                label: 'home',
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.category_outlined,
                ),
                label: 'home',
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.perm_identity,
                ),
                label: 'home',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
