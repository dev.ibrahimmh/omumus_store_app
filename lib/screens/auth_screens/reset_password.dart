import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/api/controllers/user_api_controller.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/helpers.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/pin_code_textfield.dart';
import 'package:omumus_store_app/widgets/store_text.dart';
import 'package:omumus_store_app/widgets/store_text_field.dart';

class ResetPassword extends StatefulWidget {
  // final String phoneNumber;
  //
  //
  // ResetPassword({required this.phoneNumber});

  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  late FocusNode _firstCodeFocusNode;
  late FocusNode _secondCodeFocusNode;
  late FocusNode _thirdCodeFocusNode;
  late FocusNode _fourthCodeFocusNode;

  late TextEditingController _firstCodeTextEditingController;
  late TextEditingController _secondCodeTextEditingController;
  late TextEditingController _thirdCodeTextEditingController;
  late TextEditingController _fourthCodeTextEditingController;

  late TextEditingController _passwordTextEditingController;
  late TextEditingController _confirmPasswordTextEditingController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _firstCodeFocusNode = FocusNode();
    _secondCodeFocusNode = FocusNode();
    _thirdCodeFocusNode = FocusNode();
    _fourthCodeFocusNode = FocusNode();

    _firstCodeTextEditingController = TextEditingController();
    _secondCodeTextEditingController = TextEditingController();
    _thirdCodeTextEditingController = TextEditingController();
    _fourthCodeTextEditingController = TextEditingController();

    _passwordTextEditingController = TextEditingController();
    _confirmPasswordTextEditingController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _firstCodeFocusNode.dispose();
    _secondCodeFocusNode.dispose();
    _thirdCodeFocusNode.dispose();
    _fourthCodeFocusNode.dispose();

    _firstCodeTextEditingController.dispose();
    _secondCodeTextEditingController.dispose();
    _thirdCodeTextEditingController.dispose();
    _fourthCodeTextEditingController.dispose();

    _passwordTextEditingController.dispose();
    _confirmPasswordTextEditingController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(32)),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: SizeConfig.scaleHeight(79)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    onPressed: () {
                      Get.back();
                    },
                    icon: Icon(
                      Icons.arrow_back,
                      size: SizeConfig.scaleWidth(25),
                      color: AppColors.on_boarding_text_title,
                    ),
                  ),
                  StoreText(
                    text: 'Reset Password',
                    textColor: AppColors.on_boarding_text_title,
                    fontSize: SizeConfig.scaleTextFont(20),
                    textAlign: TextAlign.center,
                    fontWeight: FontWeight.w500,
                  ),
                  StoreText(
                    text: '0',
                    textColor: Colors.transparent,
                  ),
                ],
              ),
              SizedBox(height: SizeConfig.scaleHeight(65)),
              StoreText(
                text:
                    'Enter 4 digit code verification, the code sent\n to your phone',
                textColor: AppColors.on_boarding_text_details,
                fontSize: SizeConfig.scaleTextFont(14),
                textHeight: 1.5,
              ),
              SizedBox(height: SizeConfig.scaleHeight(60)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(width: SizeConfig.scaleWidth(0)),
                  PinCodeTextField(
                    textEditingController: _firstCodeTextEditingController,
                    onChanged: (String text) {
                      if (text.length == 1) {
                        _secondCodeFocusNode.requestFocus();
                      }
                    },
                    focusNode: _firstCodeFocusNode,
                  ),
                  PinCodeTextField(
                    textEditingController: _secondCodeTextEditingController,
                    onChanged: (String text) {
                      if (text.length == 1) {
                        _thirdCodeFocusNode.requestFocus();
                      }
                    },
                    focusNode: _secondCodeFocusNode,
                  ),
                  PinCodeTextField(
                    textEditingController: _thirdCodeTextEditingController,
                    onChanged: (String text) {
                      if (text.length == 1) {
                        _fourthCodeFocusNode.requestFocus();
                      }
                    },
                    focusNode: _thirdCodeFocusNode,
                  ),
                  PinCodeTextField(
                    textEditingController: _fourthCodeTextEditingController,
                    // onChanged: (String text) {
                    //   if (text.length == 1) {
                    //     // performPasswordReset();
                    //   }
                    // },
                    focusNode: _fourthCodeFocusNode,
                  ),
                  SizedBox(width: SizeConfig.scaleWidth(0)),
                ],
              ),
              SizedBox(height: SizeConfig.scaleHeight(40)),
              StoreTextField(
                textEditingController: _passwordTextEditingController,
                labelText: 'Password',
                hintText: 'Enter your password',
                keyboardTextInputType: TextInputType.visiblePassword,
                obscureText: true,
                suffixIcon: Icon(
                  Icons.lock_outline,
                  color: AppColors.on_boarding_text_field_text,
                ),
              ),
              SizedBox(height: SizeConfig.scaleHeight(15)),
              StoreTextField(
                textEditingController: _confirmPasswordTextEditingController,
                labelText: 'Confirm Password',
                hintText: 'Confirm your password',
                keyboardTextInputType: TextInputType.visiblePassword,
                obscureText: true,
                suffixIcon: Icon(
                  Icons.lock_outline,
                  color: AppColors.on_boarding_text_field_text,
                ),
              ),
              SizedBox(height: SizeConfig.scaleHeight(45)),
              ElevatedButton(
                onPressed: () async {
                  await performPasswordReset();
                },
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(
                    double.infinity,
                    SizeConfig.scaleHeight(50),
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadiusDirectional.circular(70),
                  ),
                  primary: AppColors.sign_in_btn,
                ),
                child: StoreText(
                  text: 'Continue',
                  fontWeight: FontWeight.w500,
                  textColor: Colors.white,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future performPasswordReset() async {
    if (checkData()) {
      await resetPassword();
    }
  }

  bool checkData() {
    if (_passwordTextEditingController.text.isNotEmpty &&
        _confirmPasswordTextEditingController.text.isNotEmpty &&
        checkCode()) {
      return checkPasswordConfirmation();
    }
    Helpers.showSnackBar(
        context: context, message: 'Check required data', error: true);
    return false;
  }

  bool checkPasswordConfirmation() {
    if (_passwordTextEditingController.text ==
        _confirmPasswordTextEditingController.text) {
      return true;
    }
    Helpers.showSnackBar(
        context: context, message: 'Password not confirm', error: true);
    return false;
  }

  bool checkCode() {
    if (_firstCodeTextEditingController.text.isNotEmpty &&
        _secondCodeTextEditingController.text.isNotEmpty &&
        _thirdCodeTextEditingController.text.isNotEmpty &&
        _fourthCodeTextEditingController.text.isNotEmpty) {
      return true;
    }
    return false;
  }

  Future resetPassword() async {
    bool status = await UserApiController().resetPassword(
      context,
      password: _passwordTextEditingController.text,
      // mobile: widget.phoneNumber,
      mobile: Get.arguments,
      code: getCode(),
    );
    if(status){
      Get.back();
    }
  }

  String getCode() {
    return _firstCodeTextEditingController.text +
        _secondCodeTextEditingController.text +
        _thirdCodeTextEditingController.text +
        _fourthCodeTextEditingController.text;
  }
}
