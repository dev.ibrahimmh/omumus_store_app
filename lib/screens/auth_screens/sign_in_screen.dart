import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/api/controllers/user_api_controller.dart';
import 'package:omumus_store_app/models/User.dart';
import 'package:omumus_store_app/screens/bottom_navegationBar/main_screen.dart';
import 'package:omumus_store_app/storage/user_shared_pref.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/helpers.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/store_text.dart';
import 'package:omumus_store_app/widgets/store_text_field.dart';

class SignInScreen extends StatefulWidget {
  const SignInScreen({Key? key}) : super(key: key);

  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  late TapGestureRecognizer _tapGestureRecognizer;
  late TextEditingController _phoneNumberTextEditingController;
  late TextEditingController _passwordTextEditingController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      _tapGestureRecognizer = TapGestureRecognizer()..onTap = goToSignUp;
    });
    _phoneNumberTextEditingController = TextEditingController();
    _passwordTextEditingController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _phoneNumberTextEditingController.dispose();
    _passwordTextEditingController.dispose();
    super.dispose();
  }

  void goToSignUp() {
    Get.toNamed('/sign_up_screen');
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(32)),
        child: SingleChildScrollView(
          child: SizedBox(
            height: height,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.max,
              children: [
                SizedBox(height: SizeConfig.scaleHeight(79)),
                // Row(
                //   children: [
                //     IconButton(
                //       onPressed: () {},
                //       icon: Icon(
                //         Icons.arrow_back,
                //         size: SizeConfig.scaleWidth(25),
                //         color: AppColors.on_boarding_text_title,
                //       ),
                //     ),
                //   ],
                // ),

                StoreText(
                  text: 'Sign In',
                  textColor: AppColors.on_boarding_text_title,
                  fontSize: SizeConfig.scaleTextFont(20),
                  textAlign: TextAlign.center,
                  fontWeight: FontWeight.w500,
                ),
                SizedBox(height: SizeConfig.scaleHeight(80)),
                StoreTextField(
                  textEditingController: _phoneNumberTextEditingController,
                  labelText: 'Phone Number',
                  hintText: 'Enter your phone number',
                  keyboardTextInputType: TextInputType.phone,
                  prefixText: '+970| ',
                  suffixIcon: Icon(
                    Icons.phone,
                    color: AppColors.on_boarding_text_field_text,
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(40)),
                StoreTextField(
                  textEditingController: _passwordTextEditingController,
                  labelText: 'Password',
                  hintText: 'Enter your password',
                  keyboardTextInputType: TextInputType.visiblePassword,
                  obscureText: true,
                  suffixIcon: Icon(
                    Icons.lock_outline,
                    color: AppColors.on_boarding_text_field_text,
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(20)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                      onPressed: () {
                        Get.toNamed('/forget_password');
                      },
                      child: StoreText(
                        text: 'Forget Password?',
                        textAlign: TextAlign.end,
                        textColor: AppColors.on_boarding_text_details,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: SizeConfig.scaleHeight(45)),
                ElevatedButton(
                  onPressed: () async {
                    await performLogin();
                  },
                  style: ElevatedButton.styleFrom(
                    minimumSize: Size(
                      double.infinity,
                      SizeConfig.scaleHeight(50),
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadiusDirectional.circular(70),
                    ),
                    primary: AppColors.sign_in_btn,
                  ),
                  child: StoreText(
                    text: 'Login',
                    fontWeight: FontWeight.w500,
                    textColor: Colors.white,
                  ),
                ),
                Spacer(),
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text: 'I don\'t have an account ? ',
                    style: TextStyle(
                      color: AppColors.sign_in_text_no_account,
                      fontSize: SizeConfig.scaleTextFont(14),
                      fontWeight: FontWeight.w500,
                      fontFamily: 'Spartan',
                    ),
                    children: [
                      TextSpan(
                        recognizer: _tapGestureRecognizer,
                        text: 'Sign Up',
                        style: TextStyle(
                          color: AppColors.sign_in_btn,
                          fontSize: SizeConfig.scaleTextFont(16),
                          fontWeight: FontWeight.w500,
                          fontFamily: 'Spartan',
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(45)),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future performLogin() async {
    if (checkData()) {
      await login();
    }
  }

  bool checkData() {
    if (_phoneNumberTextEditingController.text.isNotEmpty &&
        _passwordTextEditingController.text.isNotEmpty) {
      return true;
    }
    Helpers.showSnackBar(
        context: context,
        message: 'Enter phone number & password',
        error: true);
    return false;
  }

  Future login() async {
    User? user = await UserApiController().login(context,
      _phoneNumberTextEditingController.text,
      _passwordTextEditingController.text,
    );

    if(user != null){
      UserSharedPref().save(user);
      Helpers.showSnackBar(context: context, message: 'Logged in successfully');
      Future.delayed(Duration(seconds: 2),(){
        Get.off(MainScreen(indexid: 0,));
      });
    }
  }
}
