import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/api/controllers/city_api_controller.dart';
import 'package:omumus_store_app/api/controllers/user_api_controller.dart';
import 'package:omumus_store_app/getx_controller/city_getx_controller.dart';
import 'package:omumus_store_app/models/User.dart';
import 'package:omumus_store_app/models/city.dart';
import 'package:omumus_store_app/screens/auth_screens/activate_screen.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/helpers.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/store_text.dart';
import 'package:omumus_store_app/widgets/store_text_field.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  late TapGestureRecognizer _tapGestureRecognizer;

  late TextEditingController _nameTextEditingController;
  late TextEditingController _mobileTextEditingController;
  late TextEditingController _passwordTextEditingController;

// late Future<List<City>> _citiesFuture;
// List<City> _cities = [];
// CityGetxController controller = Get.put(CityGetxController());

  // List<String> _locations = ['A', 'B', 'C', 'D'];
  // String _selectedLocation = 'A';

  List<String> genderList = ['M', 'F'];
  String _selectGender = 'M';
  int cityId = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      _tapGestureRecognizer = TapGestureRecognizer()..onTap = goToSignIn;
      // _citiesFuture = CityApiController().cities();
    });

    _nameTextEditingController = TextEditingController();
    _mobileTextEditingController = TextEditingController();
    _passwordTextEditingController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _nameTextEditingController.dispose();
    _mobileTextEditingController.dispose();
    _passwordTextEditingController.dispose();
    super.dispose();
  }

  void goToSignIn() {
    Get.offNamed('/sign_in_screen');
  }



  @override
  Widget build(BuildContext context) {
    // String? genderValue;
    Map<int, String> mappedGender = genderList.asMap();
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(25)),
        child: SingleChildScrollView(
          child: SizedBox(
            height: height,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.max,
              children: [
                SizedBox(height: SizeConfig.scaleHeight(79)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      onPressed: () {
                        Get.back();
                      },
                      icon: Icon(
                        Icons.arrow_back,
                        size: SizeConfig.scaleWidth(25),
                        color: AppColors.on_boarding_text_title,
                      ),
                    ),
                    StoreText(
                      text: 'Sign Up',
                      textColor: AppColors.on_boarding_text_title,
                      fontSize: SizeConfig.scaleTextFont(20),
                      textAlign: TextAlign.center,
                      fontWeight: FontWeight.w500,
                    ),
                    StoreText(
                      text: '0',
                      textColor: Colors.transparent,
                    ),
                  ],
                ),
                SizedBox(height: SizeConfig.scaleHeight(90)),
                StoreTextField(
                  textEditingController: _nameTextEditingController,
                  labelText: 'Username',
                  hintText: 'Enter your name',
                  // keyboardTextInputType: TextInputType.phone,
                  suffixIcon: Icon(
                    Icons.perm_identity,
                    color: AppColors.on_boarding_text_field_text,
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(30)),
                StoreTextField(
                  textEditingController: _mobileTextEditingController,
                  labelText: 'Phone number',
                  hintText: 'Enter your phone number',
                  keyboardTextInputType: TextInputType.phone,
                  prefixText: '+970| ',
                  suffixIcon: Icon(
                    Icons.phone,
                    color: AppColors.on_boarding_text_field_text,
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(30)),
                StoreTextField(
                  textEditingController: _passwordTextEditingController,
                  labelText: 'Password',
                  hintText: 'Enter your password',
                  keyboardTextInputType: TextInputType.visiblePassword,
                  obscureText: true,
                  suffixIcon: Icon(
                    Icons.lock_outline,
                    color: AppColors.on_boarding_text_field_text,
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(30)),
                Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig.scaleWidth(15),
                      vertical: SizeConfig.scaleHeight(5)),
                  decoration: BoxDecoration(
                    color:
                        AppColors.on_boarding_text_field_text.withOpacity(0.2),
                    borderRadius: BorderRadius.circular(60),
                  ),
                  child:  GetX<CityGetxController>(
                    // init: CityGetxController(),
                      builder : (CityGetxController controller){
                        if(controller.city.isNotEmpty){
                          print('city_number ${controller.city.length}');
                          return DropdownButtonFormField<City>(
                            items: controller.city.value.map((e) => DropdownMenuItem<City>(
                              child: Text(e.nameEn.toString()),
                              // value: e.nameEn,
                              value: e,
                            )).toList(),
                            value: controller.city.value.elementAt(0),
                            onChanged: (City? city){
                              setState(() {
                                cityId = city!.id;
                              });
                            },
                          );

                        }else{
                          print(controller.city.length);

                          return Text("");
                        }
                      }),
                ),
                SizedBox(height: SizeConfig.scaleHeight(30)),

                StatefulBuilder(
                  builder: (_, StateSetter setState) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          'Gender : ',
                          style: TextStyle(fontWeight: FontWeight.w400),
                        ),
                        ...mappedGender.entries.map(
                          (MapEntry<int, String> mapEntry) => Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Radio(
                                  activeColor:
                                      AppColors.main_screen_selected_icon,
                                  groupValue: _selectGender,
                                  value: genderList[mapEntry.key],
                                  onChanged: (value) {
                                    setState(() {
                                      _selectGender = value as String;
                                    });
                                  },
                                ),
                                Text(mapEntry.value)
                              ]),
                        ),
                      ],
                    );
                  },
                ),

                // GenderField(
                //   genderList: ['Male', 'Female'],
                // ),
                SizedBox(height: SizeConfig.scaleHeight(45)),
                ElevatedButton(
                  onPressed: () async{
                    await performRegister();
                  },
                  style: ElevatedButton.styleFrom(
                    minimumSize: Size(
                      double.infinity,
                      SizeConfig.scaleHeight(50),
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadiusDirectional.circular(70),
                    ),
                    primary: AppColors.sign_in_btn,
                  ),
                  child: StoreText(
                    text: 'Sign Up',
                    fontWeight: FontWeight.w500,
                    textColor: Colors.white,
                  ),
                ),
                Spacer(),
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text: 'Already have an account ? ',
                    style: TextStyle(
                      color: AppColors.sign_in_text_no_account,
                      fontSize: SizeConfig.scaleTextFont(14),
                      fontWeight: FontWeight.w500,
                      fontFamily: 'Spartan',
                    ),
                    children: [
                      TextSpan(
                        recognizer: _tapGestureRecognizer,
                        text: 'Sign In',
                        style: TextStyle(
                          color: AppColors.sign_in_btn,
                          fontSize: SizeConfig.scaleTextFont(16),
                          fontWeight: FontWeight.w500,
                          fontFamily: 'Spartan',
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(45)),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future performRegister() async {
    if(checkData()){
      await register();
    }
  }

  bool checkData() {
    if (_nameTextEditingController.text.isNotEmpty &&
        _mobileTextEditingController.text.isNotEmpty &&
        _passwordTextEditingController.text.isNotEmpty &&
        cityId != 0 &&
        _selectGender != null) {
      return true;
    }
    Helpers.showSnackBar(
        context: context, message: 'Check required data', error: true);
    return false;
  }

  Future register() async {
    bool status = await UserApiController().register(
      context,
      username: _nameTextEditingController.text,
      mobile: _mobileTextEditingController.text,
      password: _passwordTextEditingController.text,
      gender: _selectGender,
      cityId: cityId,
    );
    if(status){
      // Helpers.showSnackBar(context: context, message: 'Logged in successfully');
        Get.off(
          ActivateScreen(phoneNumber: _mobileTextEditingController.text,),
          // arguments: _mobileTextEditingController.text,
        );
    }
  }
}

/*
*
* SingleChildScrollView(
          child: SizedBox(
            height: height,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.max,
              children: [
                SizedBox(height: SizeConfig.scaleHeight(79)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      onPressed: () {
                        Get.back();
                      },
                      icon: Icon(
                        Icons.arrow_back,
                        size: SizeConfig.scaleWidth(25),
                        color: AppColors.on_boarding_text_title,
                      ),
                    ),
                    StoreText(
                      text: 'Sign Up',
                      textColor: AppColors.on_boarding_text_title,
                      fontSize: SizeConfig.scaleTextFont(20),
                      textAlign: TextAlign.center,
                      fontWeight: FontWeight.w500,
                    ),
                    StoreText(
                      text: '0',
                      textColor: Colors.transparent,
                    ),
                  ],
                ),
                SizedBox(height: SizeConfig.scaleHeight(90)),
                StoreTextField(
                  textEditingController: _nameTextEditingController,
                  labelText: 'Username',
                  hintText: 'Enter your name',
                  // keyboardTextInputType: TextInputType.phone,
                  suffixIcon: Icon(
                    Icons.perm_identity,
                    color: AppColors.on_boarding_text_field_text,
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(30)),
                StoreTextField(
                  textEditingController: _mobileTextEditingController,
                  labelText: 'Phone number',
                  hintText: 'Enter your phone number',
                  keyboardTextInputType: TextInputType.phone,
                  prefixText: '+970| ',
                  suffixIcon: Icon(
                    Icons.phone,
                    color: AppColors.on_boarding_text_field_text,
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(30)),
                StoreTextField(
                  textEditingController: _passwordTextEditingController,
                  labelText: 'Password',
                  hintText: 'Enter your password',
                  keyboardTextInputType: TextInputType.visiblePassword,
                  obscureText: true,
                  suffixIcon: Icon(
                    Icons.lock_outline,
                    color: AppColors.on_boarding_text_field_text,
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(30)),
                Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig.scaleWidth(15),
                      vertical: SizeConfig.scaleHeight(5)),
                  decoration: BoxDecoration(
                    color:
                        AppColors.on_boarding_text_field_text.withOpacity(0.2),
                    borderRadius: BorderRadius.circular(60),
                  ),
                  child: DropdownButton(
                    hint: StoreText(
                      text: 'Please choose a location',
                      textColor: AppColors.on_boarding_text_field_text,
                    ),
                    dropdownColor: AppColors.on_boarding_text_field_text,
                    isExpanded: true,
                    iconSize: 30.0,
                    value: _selectedLocation,
                    onChanged: (String? val) {
                      setState(() {
                        _selectedLocation = val;
                      });
                    },
                    items: _locations.map((location) {
                      return DropdownMenuItem(
                        child: new Text(location),
                        value: location,
                      );
                    }).toList(),
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(30)),

                StatefulBuilder(
                  builder: (_, StateSetter setState) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          'Gender : ',
                          style: TextStyle(fontWeight: FontWeight.w400),
                        ),
                        ...mappedGender.entries.map(
                          (MapEntry<int, String> mapEntry) => Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Radio(
                                  activeColor:
                                      AppColors.main_screen_selected_icon,
                                  groupValue: select,
                                  value: genderList[mapEntry.key],
                                  onChanged: (value) {
                                    setState(() {
                                      select = value as String?;
                                    });
                                  },
                                ),
                                Text(mapEntry.value)
                              ]),
                        ),
                      ],
                    );
                  },
                ),

                // GenderField(
                //   genderList: ['Male', 'Female'],
                // ),
                SizedBox(height: SizeConfig.scaleHeight(45)),
                ElevatedButton(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                    minimumSize: Size(
                      double.infinity,
                      SizeConfig.scaleHeight(50),
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadiusDirectional.circular(70),
                    ),
                    primary: AppColors.sign_in_btn,
                  ),
                  child: StoreText(
                    text: 'Sign Up',
                    fontWeight: FontWeight.w500,
                    textColor: Colors.white,
                  ),
                ),
                Spacer(),
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text: 'Already have an account ? ',
                    style: TextStyle(
                      color: AppColors.sign_in_text_no_account,
                      fontSize: SizeConfig.scaleTextFont(14),
                      fontWeight: FontWeight.w500,
                      fontFamily: 'Spartan',
                    ),
                    children: [
                      TextSpan(
                        recognizer: _tapGestureRecognizer,
                        text: 'Sign In',
                        style: TextStyle(
                          color: AppColors.sign_in_btn,
                          fontSize: SizeConfig.scaleTextFont(16),
                          fontWeight: FontWeight.w500,
                          fontFamily: 'Spartan',
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(45)),
              ],
            ),
          ),
        ),
* */
