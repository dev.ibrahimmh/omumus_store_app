import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/api/controllers/user_api_controller.dart';
import 'package:omumus_store_app/screens/auth_screens/reset_password.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/helpers.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/store_text.dart';
import 'package:omumus_store_app/widgets/store_text_field.dart';

class ForgetPassword extends StatefulWidget {
  const ForgetPassword({Key? key}) : super(key: key);

  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  late TextEditingController _phoneTextEditingController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _phoneTextEditingController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _phoneTextEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(32)),
        child: Column(
          children: [
            SizedBox(height: SizeConfig.scaleHeight(79)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(
                    Icons.arrow_back,
                    size: SizeConfig.scaleWidth(25),
                    color: AppColors.on_boarding_text_title,
                  ),
                ),
                StoreText(
                  text: 'Forget Password',
                  textColor: AppColors.on_boarding_text_title,
                  fontSize: SizeConfig.scaleTextFont(20),
                  textAlign: TextAlign.center,
                  fontWeight: FontWeight.w500,
                ),
                StoreText(
                  text: '0',
                  textColor: Colors.transparent,
                ),
              ],
            ),
            SizedBox(height: SizeConfig.scaleHeight(65)),
            StoreText(
              text:
                  'Please enter your phone Number and we\n will  send you a code to your phone  to\n rest the password',
              textColor: AppColors.on_boarding_text_details,
              fontSize: SizeConfig.scaleTextFont(14),
            ),
            SizedBox(height: SizeConfig.scaleHeight(60)),
            StoreTextField(
              textEditingController: _phoneTextEditingController,
              labelText: 'Phone number',
              hintText: 'Enter your phone number',
              keyboardTextInputType: TextInputType.phone,
              prefixText: '+970| ',
              suffixIcon: Icon(
                Icons.phone,
                color: AppColors.on_boarding_text_field_text,
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(45)),
            ElevatedButton(
              onPressed: () async {
                await performForgetPassword();
              },
              style: ElevatedButton.styleFrom(
                minimumSize: Size(
                  double.infinity,
                  SizeConfig.scaleHeight(50),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadiusDirectional.circular(70),
                ),
                primary: AppColors.sign_in_btn,
              ),
              child: StoreText(
                text: 'Continue',
                fontWeight: FontWeight.w500,
                textColor: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future performForgetPassword() async {
    if (checkData()) {
      await forgetPassword();
    }
  }

  bool checkData() {
    if (_phoneTextEditingController.text.isNotEmpty) {
      return true;
    }
    Helpers.showSnackBar(
        context: context, message: 'Enter phone number', error: true);
    return false;
  }

  Future forgetPassword() async {
    bool status = await UserApiController()
        .forgetPassword(context, mobile: _phoneTextEditingController.text);
    if (status) {
      Get.off(
        ResetPassword(),
        arguments: _phoneTextEditingController.text,
      );
    }
  }
}
