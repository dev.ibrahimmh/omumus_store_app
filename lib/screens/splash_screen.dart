import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/storage/user_shared_pref.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/store_text.dart';


class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 3),(){
      String route = UserSharedPref().isLoggedIn() ? '/main_screen' : '/on_boarding_screen';
      Get.offNamed(route);
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: AppColors.BACKGROUND_SPLASH,
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(
              'images/splash_logo.png',
            ),
            SizedBox(height: SizeConfig.scaleHeight(23)),
            StoreText(
              text: 'Omumu\s',
              fontWeight: FontWeight.w600,
              textColor: Colors.white,
              fontSize: 36,
            ),
          ],
        ),
      ),
    );
  }
}
