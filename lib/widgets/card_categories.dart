import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/store_text.dart';

class CardCategories extends StatelessWidget {
  final String categoryTitle;
  final String imagePath;
  final VoidCallback? goSubCategory;

  CardCategories({
    required this.categoryTitle,
    required this.imagePath,
    this.goSubCategory,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: goSubCategory,
      child: SizedBox(
        height: SizeConfig.scaleHeight(150),
        child: Card(
          color: AppColors.BACKGROUND_SPLASH.withOpacity(0.5),
          margin: EdgeInsets.only(bottom: SizeConfig.scaleHeight(15)),
          elevation: 3,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          child: Padding(
            padding: EdgeInsetsDirectional.only(
              start: SizeConfig.scaleWidth(15),
              end: SizeConfig.scaleWidth(10),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                StoreText(
                  text: categoryTitle,
                  textColor: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                ),
                Container(
                  height: SizeConfig.scaleHeight(100),
                  width: SizeConfig.scaleWidth(100),
                  child: CachedNetworkImage(
                    height: 172,
                    width: double.infinity,
                    imageUrl: imagePath,
                    placeholder: (context, url) => Center(
                      child: CircularProgressIndicator(
                        color: AppColors.main_screen_selected_icon,
                      ),
                    ),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                    fit: BoxFit.cover,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
 //Image.network(imagePath)