import 'package:flutter/material.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/size_config.dart';

class OnBoardingIndicator extends StatelessWidget {
  final bool selected;

  OnBoardingIndicator({
    required this.selected,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5),
      width: selected ? SizeConfig.scaleWidth(13) : SizeConfig.scaleWidth(10),
      height: selected ? SizeConfig.scaleHeight(13) : SizeConfig.scaleHeight(10),
      decoration: BoxDecoration(
        color: selected ? AppColors.on_boarding_text_next : AppColors.on_boarding_indicator_unselected,
        shape: BoxShape.circle,
        // border: selected ? Border.all(
        //   width: SizeConfig.scaleWidth(1),
        //   color: AppColors.on_boarding_text_next,
        // ) : null,
      ),
    );
  }
}
