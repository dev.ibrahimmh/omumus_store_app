import 'package:flutter/material.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/store_text.dart';

class ListTileProfile extends StatelessWidget {
  final String title;
  final Widget? leading;
  final Widget? trailing;
  final GestureTapCallback? onTap;
  final Widget? subTitle;

  ListTileProfile({
    required this.title,
    this.leading,
    this.trailing,
    this.onTap,
    this.subTitle,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      horizontalTitleGap: 0,
      onTap: onTap,
      contentPadding: EdgeInsetsDirectional.only(
        start: SizeConfig.scaleWidth(20),
        end: SizeConfig.scaleWidth(20),
        top: SizeConfig.scaleHeight(10),
        bottom: SizeConfig.scaleHeight(10),
      ),
      title: StoreText(
        text: title,
        textAlign: TextAlign.start,
        fontSize: 18,
        fontWeight: FontWeight.w500,
        textColor: AppColors.main_screen_selected_icon,
      ),
      leading: leading,
      trailing: trailing,
      subtitle: subTitle,
    );
  }
}
