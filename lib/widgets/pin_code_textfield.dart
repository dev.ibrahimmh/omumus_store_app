import 'package:flutter/material.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/size_config.dart';

class PinCodeTextField extends StatelessWidget {
  final FocusNode? focusNode;
  final ValueChanged<String>? onChanged;
  final TextEditingController? textEditingController;

  PinCodeTextField({
    this.focusNode,
    this.onChanged,
    this.textEditingController,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.scaleHeight(45),
      width: SizeConfig.scaleWidth(45),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Color(0xFFD6D6D6).withAlpha(40),
            offset: Offset(0, 0),
            blurRadius: 10,
            spreadRadius: 1,
          ),
        ],
      ),
      child: TextField(
        controller: textEditingController,
        cursorWidth: 1,
        minLines: null,
        maxLines: null,
        expands: true,
        maxLength: 1,
        keyboardType: TextInputType.phone,
        focusNode: focusNode,
        onChanged: onChanged,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 23,
          fontWeight: FontWeight.bold,
          color: AppColors.sign_in_btn,
          fontFamily: 'Spartan',
        ),
        decoration: InputDecoration(
          counterText: '',
          filled: true,
          fillColor: Color(0xFFFCFCFC),
          // focusNode!.hasFocus ? Color(0xFF472FC8) : Colors.white,
          contentPadding: EdgeInsets.zero,
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: AppColors.sign_in_text_field_border,
            ),
            borderRadius: BorderRadius.circular(
              SizeConfig.scaleWidth(11),
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: AppColors.sign_in_text_field_border,
            ),
            borderRadius: BorderRadius.circular(
              SizeConfig.scaleWidth(11),
            ),
          ),
        ),
      ),
    );
  }
}
