import 'package:flutter/material.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/size_config.dart';

class TextFieldSearch extends StatelessWidget {
  final TextEditingController? textEditingController;

  TextFieldSearch({
    this.textEditingController,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      cursorColor: AppColors.on_boarding_text_field_border,
      cursorHeight: SizeConfig.scaleHeight(20),
      cursorWidth: SizeConfig.scaleWidth(1),
      controller: textEditingController,
      style: TextStyle(
        fontSize: SizeConfig.scaleTextFont(15),
        color: AppColors.on_boarding_text_details,
      ),
      decoration: InputDecoration(
        fillColor: AppColors.main_screen_textField,
        filled: true,
        contentPadding: EdgeInsetsDirectional.only(
          start: SizeConfig.scaleWidth(28),
          end: SizeConfig.scaleWidth(16),
          top: SizeConfig.scaleHeight(18),
          bottom: SizeConfig.scaleHeight(18),
        ),
        labelStyle: TextStyle(
          color: AppColors.on_boarding_text_field_text,
          fontSize: SizeConfig.scaleTextFont(15),
        ),
        hintText: 'Search...',
        hintStyle: TextStyle(
          color: AppColors.on_boarding_text_field_text,
          fontSize: SizeConfig.scaleTextFont(15),
        ),
        suffixIcon: Icon(
          Icons.search_rounded,
          color: AppColors.main_screen_un_selected_icon,
        ),
        // prefixIcon: prefixIcon,
        errorBorder: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(SizeConfig.scaleWidth(70))),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(SizeConfig.scaleWidth(70))),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(
            SizeConfig.scaleWidth(70),
          ),
        ),
      ),
    );
  }
}
