import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/getx_controller/product_getx_controller.dart';
import 'package:omumus_store_app/models/product.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/store_text.dart';

class ProductCard extends StatelessWidget {
  ProductGetxController controller = Get.put(ProductGetxController());
  final Product product;
  final void Function() onTap;
  ProductCard({required this.product, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Card(
        clipBehavior: Clip.antiAlias,
        color: Colors.white,
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        child: Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: CachedNetworkImage(
                    height: 172,
                    width: double.infinity,
                    imageUrl: product.imageUrl,
                    alignment: Alignment.center,
                    placeholder: (context, url) => Center(
                      child: CircularProgressIndicator(
                        color: AppColors.main_screen_selected_icon,
                      ),
                    ),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      StoreText(
                        text: product.nameEn,
                        fontSize: 12,
                        textColor: Colors.grey,
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      StoreText(
                        text: '${product.price}\$',
                        fontSize: 15,
                        textColor: AppColors.on_boarding_text_title,
                        fontWeight: FontWeight.bold,
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                )
              ],
            ),
            Positioned(
              top: SizeConfig.scaleHeight(10),
              right: SizeConfig.scaleWidth(10),
              child: Container(
                height: 45,
                width: 45,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: ProductGetxController.to.favoriteProducts.where((element) => element.id == product.id).isNotEmpty ? Colors.red : Colors.grey,
                ),
                child: Icon(
                  Icons.favorite,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
