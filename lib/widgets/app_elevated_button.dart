
import 'package:flutter/material.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/store_text.dart';


class AppElevatedButton extends StatelessWidget {
  final String text;
  final Color textColor;
  final Color buttonColor;
  final FontWeight fontWeight;
  final double fontSize;
  final TextAlign textAlign;
  final void Function() onPressed;

  AppElevatedButton(
      {required this.text,
      this.textColor = Colors.white,
      this.fontWeight = FontWeight.bold,
      this.fontSize = 16,
      this.textAlign = TextAlign.start,
      this.buttonColor = AppColors.sign_in_btn,
      required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      child: StoreText(
        text: text,

        textColor: textColor,
        fontWeight: fontWeight,
        fontSize: fontSize,
      ),
      style: ElevatedButton.styleFrom(
        minimumSize: Size(double.infinity, SizeConfig.scaleWidth(50)),
        primary: buttonColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
           25
          ),
        ),
      ),
      onPressed: onPressed,
    );
  }
}
