import 'package:flutter/material.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/size_config.dart';
import 'package:omumus_store_app/widgets/store_text.dart';

class OnBoardingContent extends StatelessWidget {
  final String imagePath;
  final String title;
  final String details;
  final VoidCallback? onPressedNext;

  OnBoardingContent({
    required this.imagePath,
    required this.title,
    required this.details,
    this.onPressedNext,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.asset(
          imagePath,
          // 'images/on_boarding1.png',
          width: double.infinity,
          height: SizeConfig.scaleHeight(500),
          // fit: BoxFit.cover,
        ),
        Positioned(
          bottom: 0,
          left: 0,
          right: 0,
          child: Container(
            width: double.infinity,
            height: SizeConfig.scaleHeight(338),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadiusDirectional.only(
                  topStart: Radius.circular(35), topEnd: Radius.circular(35)),
            ),
            child: Column(
              children: [
                SizedBox(height: SizeConfig.scaleHeight(78)),
                StoreText(
                  text: title, // 'Great deals',
                  fontSize: 26,
                  textColor: AppColors.on_boarding_text_title,
                ),
                SizedBox(height: SizeConfig.scaleHeight(13)),
                StoreText(
                  text: details,
                  // 'D’ont miss out our flash sale  and daily specials with great discounts',
                  fontSize: 15,
                  textColor: AppColors.on_boarding_text_details,
                  textAlign: TextAlign.center,
                  textHeight: 1.2,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
