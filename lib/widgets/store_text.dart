import 'package:flutter/material.dart';
import 'package:omumus_store_app/utils/size_config.dart';

class StoreText extends StatelessWidget {
  final String text;
  final Color textColor;
  final FontWeight fontWeight;
  final double fontSize;
  final TextAlign textAlign;
  final double? textHeight;

  StoreText({
    required this.text,
    this.textColor = Colors.black,
    this.fontWeight = FontWeight.normal,
    this.fontSize = 16,
    this.textAlign = TextAlign.start,
    this.textHeight,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlign,
      style: TextStyle(
        color: textColor,
        fontSize: SizeConfig.scaleTextFont(fontSize),
        fontFamily: 'Spartan',
        fontWeight: fontWeight,
        height: textHeight,
      ),
    );
  }
}
