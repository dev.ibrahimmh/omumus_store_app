import 'package:flutter/material.dart';
import 'package:omumus_store_app/utils/app_color.dart';
import 'package:omumus_store_app/utils/size_config.dart';


class StoreTextField extends StatelessWidget {

  final bool obscureText;
  final TextEditingController? textEditingController;
  final TextInputType keyboardTextInputType;
  final String labelText;
  final String hintText;
  final Widget? suffixIcon;
  final String? prefixText;


  StoreTextField({
    this.obscureText = false,
    this.textEditingController,
    this.keyboardTextInputType = TextInputType.name,
    required this.labelText,
    required this.hintText,
    this.suffixIcon,
    this.prefixText,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      cursorColor: AppColors.on_boarding_text_field_border,
      cursorHeight: SizeConfig.scaleHeight(20),
      cursorWidth: SizeConfig.scaleWidth(1),
      obscureText: obscureText,
      controller: textEditingController,
      keyboardType: keyboardTextInputType,
      style: TextStyle(
        fontSize: SizeConfig.scaleTextFont(15),
        color: AppColors.on_boarding_text_details,
      ),
      decoration: InputDecoration(
        // fillColor: AppColors.on_boarding_text_field_text,
        // filled: true,
        contentPadding: EdgeInsetsDirectional.only(
          start: SizeConfig.scaleWidth(28),
          end: SizeConfig.scaleWidth(16),
          top: SizeConfig.scaleHeight(18),
          bottom: SizeConfig.scaleHeight(18),
        ),
        labelText: labelText,
        labelStyle: TextStyle(
          color: AppColors.on_boarding_text_field_text,
          fontSize: SizeConfig.scaleTextFont(15),
        ),
        hintText: hintText,
        hintStyle: TextStyle(
          color: AppColors.on_boarding_text_field_text,
          fontSize: SizeConfig.scaleTextFont(15),
        ),
        suffixIcon: suffixIcon,
        // prefixIcon: prefixIcon,
        prefixText: prefixText,
        errorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Colors.red,
            ),
            borderRadius:
            BorderRadius.circular(SizeConfig.scaleWidth(70))),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: AppColors.on_boarding_text_field_border,
            ),
            borderRadius:
            BorderRadius.circular(SizeConfig.scaleWidth(70))),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: AppColors.on_boarding_text_field_border,
            width: 1,
          ),
          borderRadius: BorderRadius.circular(
            SizeConfig.scaleWidth(70),
          ),
        ),
      ),
    );
  }
}