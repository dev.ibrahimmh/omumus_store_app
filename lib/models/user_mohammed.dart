

class UserMohammed {
 late int id;
 late String name;
 late String mobile;
 late String gender;
 late int cityId;
 late String token;

 UserMohammed({
        required this.id,
        required this.name,
        required this.mobile,
        required this.gender,
        required this.cityId,
        required this.token,
      });

 UserMohammed.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    mobile = json['mobile'];
    gender = json['gender'];
    cityId = json['city_id'];
    token = 'Bearer ${json['token']}';
  }

   Map<String, dynamic> toJson() {
     final Map<String, dynamic> data = new Map<String, dynamic>();
     data['id'] = this.id;
     data['name'] = this.name;
     data['mobile'] = this.mobile;
     data['gender'] = this.gender;
     data['city_id'] = this.cityId;
     data['token'] = this.token;
     return data;
   }
}
