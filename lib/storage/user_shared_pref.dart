import 'package:omumus_store_app/models/User.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserSharedPref{

  static final UserSharedPref _instance = UserSharedPref._internal();

  late final SharedPreferences _sharedPreferences;

  factory UserSharedPref(){
    return _instance;
  }

  UserSharedPref._internal();

  Future<void> initSharedPref() async{
    _sharedPreferences = await SharedPreferences.getInstance();
  }


  String get languageCode => _sharedPreferences.getString('language_code') ?? 'en';

  Future<bool> setLanguage(String languageCode) async {
    return await _sharedPreferences.setString('language_code', languageCode);
  }

  Future  save(User user) async{
    await _sharedPreferences.setBool('logged_in', true);
    await _sharedPreferences.setInt('id', user.id);
    await _sharedPreferences.setString('name', user.name);
    await _sharedPreferences.setString('mobile', user.mobile);
    await _sharedPreferences.setString('gender', user.gender);
    await _sharedPreferences.setBool('active', user.active);
    await _sharedPreferences.setInt('city_id', user.cityId);
    await _sharedPreferences.setString('token', user.token);
  }

  String getName(){
    String name = _sharedPreferences.getString('name') ?? '';
    return name;
  }

  int getId(){
    int id =_sharedPreferences.getInt('id') ?? 0 ;
    return id;
  }

  bool isLoggedIn(){
    return _sharedPreferences.getBool('logged_in') ?? false;
  }


  String getToken(){
    String token = _sharedPreferences.getString('token') ?? '';
    return 'Bearer $token';
  }

  Future<bool> logout()async{
    return await _sharedPreferences.clear();
  }
}