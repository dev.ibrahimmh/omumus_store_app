import 'package:get/get.dart';
import 'package:omumus_store_app/api/controllers/city_api_controller.dart';
import 'package:omumus_store_app/models/city.dart';

class CityGetxController  extends GetxController{
  RxList<City> city = <City>[].obs;

  CityApiController cityController = CityApiController();

  static CityGetxController get to => Get.find();

  @override
  void onInit() {
    // TODO: implement onInit
    readCities();
    super.onInit();
  }
  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    city.clear();
    super.onClose();
  }


  Future<void> readCities() async{
    print("Before");
    city.value = await cityController.cities();
    print("After");
    print(city);
  }


}