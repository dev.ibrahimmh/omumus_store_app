import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/api/controllers/category_api_controller.dart';
import 'package:omumus_store_app/models/category.dart';
import 'package:omumus_store_app/models/sub_category.dart';


class CategoryGetxController extends GetxController {
  final CategoryApiController categoryApiController = CategoryApiController();
  RxList<Category> categories = <Category>[].obs;
  RxList<SubCategory> subCategories = <SubCategory>[].obs;

  static CategoryGetxController get to => Get.find();

  void onInit() {
    getCategory();
    super.onInit();
  }


  @override
  void onClose() {
    // TODO: implement onClose
    categories.clear();
    subCategories.clear();
    super.onClose();
  }

  Future<void> getCategory() async {
    categories.value = await categoryApiController.categories();
    update();
  }


  Future<void> getSubCategories({required int id}) async {
    subCategories.value = await categoryApiController.subcategories(id: id);
    update();
  }
}
