

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/api/controllers/product_api_controller.dart';
import 'package:omumus_store_app/models/product.dart';
import 'package:omumus_store_app/models/product_details.dart';

class ProductGetxController extends GetxController{

  final ProductApiController productApiController = ProductApiController();
  RxList<Product> products = <Product>[].obs;
  RxList<ProductDetails?> productsDetails = <ProductDetails>[].obs;
  RxList<Product> favoriteProducts = <Product>[].obs;

  static ProductGetxController get to => Get.find();

  Future<void> getProduct({required int id}) async {
    products.value = await productApiController.products(id: id);
    products.refresh();
  }

  Future<void> getProductDetails(int id) async {

    ProductDetails? productDetails = await productApiController.productsDetails(id);
    productsDetails.add(productDetails);
    productsDetails.refresh();
  }

  Future<void> getFavoriteProducts() async {
    favoriteProducts.value = await productApiController.getFavoriteProducts();
    favoriteProducts.refresh();
  }

  void onInit() {
    getFavoriteProducts();
    super.onInit();
  }

  Future<void> addFavoriteProducts({required int id,required BuildContext context}) async {
    await productApiController.addFavoriteProducts(context, id: id);

    if (favoriteProducts
        .where((element) => element.id == id)
        .isNotEmpty) {
      if (favoriteProducts
          .firstWhere((element) => element.id == id).isFavorite) {
        favoriteProducts
            .firstWhere((element) => element.id == id)
            .isFavorite = false;
      } else {
        favoriteProducts
            .firstWhere((element) => element.id == id)
            .isFavorite = true;
      }
    }
    getFavoriteProducts();
    favoriteProducts.refresh();
  }

  Future<void> rattingProduct({required int productId,required BuildContext context,required double rate}) async {
    bool status = await productApiController.productRate(context, id: productId,ratting: rate);
    if(status){
      int index = products.indexWhere((element) => element.id == productId);
      products[index].productRate = rate;
    }
    update();
  }

}