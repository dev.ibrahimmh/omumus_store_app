

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/api/controllers/product_api_controller.dart';
import 'package:omumus_store_app/models/product.dart';
import 'package:omumus_store_app/models/product_details.dart';

class ProductDetailsGetxController extends GetxController{

  final ProductApiController productApiController = ProductApiController();

  RxList<ProductDetails?> productsDetails = <ProductDetails>[].obs;


  static ProductDetailsGetxController get to => Get.find();



  Future<void> getProductDetails(int id) async {
    ProductDetails? productDetails = await productApiController.productsDetails(id);
    productsDetails.add(productDetails);
    productsDetails.refresh();
  }




}