import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/api/controllers/address_api_controller.dart';
import 'package:omumus_store_app/models/address.dart';


class AddressGetxController extends GetxController{

  RxList<Address> addresses = <Address>[].obs;
  // RxBool isSelected = false.obs;
  RxBool isSelected = false.obs;

  AddressApiController addressApiController = AddressApiController();
  static AddressGetxController get to => Get.find();

  @override
  void onInit() {
    // TODO: implement onInit
    readAddress();
    isSelected.value;
    super.onInit();
  }
  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    addresses.clear();
    super.onClose();
  }


  Future<bool> addAddress(BuildContext context , {required String addressName , required String addressInfo, required String contactNumber , required int cityId}) async{
    Address? addressObj = await addressApiController.addAddress(context, addressName: addressName, addressInfo: addressInfo, contactNumber: contactNumber, cityId: cityId);
    if(addressObj != null){
       // addressObj.id = address.id;
      addresses.add(addressObj);
      await addressApiController.addAddress(context, addressName: addressName, addressInfo: addressInfo, contactNumber: contactNumber, cityId: cityId);
    }
    return addressObj != null;
  }

  Future<void> readAddress() async{
    addresses.value = await addressApiController.indexAddresses();
  }

  Future<bool> updateAddress(BuildContext context ,{required Address address}) async{
    bool update  = await addressApiController.updateAddress(context, address: address);
    int index = addresses.indexWhere((element) => element.id == address.id);
    if(index != -1) addresses[index] = address;
    return update;

  }


  Future<bool> deleteAddress(BuildContext context , {required int id}) async{
    bool deleted = await addressApiController.deleteAddress(context, id : id);
    int index = addresses.indexWhere((element) => element.id == id);
    if(index != -1) addresses.removeAt(index);
    return deleted;

  }

  //
  // bool selectAddress({required int idAddress}) {
  //   addresses.forEach((element) {
  //     element.isSelected = false;
  //   });
  //   int index = addresses.indexWhere((element) => element.id == idAddress);
  //   if(index != -1){
  //     isSelected.value = true;
  //     addresses[index].isSelected = true;
  //     return true;
  //   }else{
  //     return false;
  //   }
  // }

  // int getAddressId(){
  //   int index = addresses.indexWhere((element) => element.isSelected == true);
  //
  //   return isSelected.value ? addresses[index].id : 0;

  // }

}