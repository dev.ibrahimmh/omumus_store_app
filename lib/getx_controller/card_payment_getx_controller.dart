

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/api/controllers/card_api_controller.dart';
import 'package:omumus_store_app/models/card_payment.dart';

class CardPaymentGetxController extends GetxController{

  RxList<CardPayment> paymentCards = <CardPayment>[].obs;
  RxBool isSelected = false.obs;

  CardPaymentApiController cardPaymentApiController = CardPaymentApiController();

  static CardPaymentGetxController get to => Get.find();

  @override
  void onInit() {
    // TODO: implement onInit
    readCardPayment();
    isSelected.value;
    super.onInit();
  }
  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    paymentCards.clear();
    super.onClose();
  }


  Future<bool> addCardPayment(BuildContext context , {required String holderName , required String cardNumber, required String expData , required String cvvNum, required String type}) async{
    CardPayment? cardPaymentObj = await cardPaymentApiController.addCardPayment(context, holderName: holderName, cardNumber: cardNumber, expData: expData, cvvNum: cvvNum, type: type);
    if(cardPaymentObj != null){
      // addressObj.id = address.id;
      paymentCards.add(cardPaymentObj);
      // await addressApiController.addAddress(context, addressName: addressName, addressInfo: addressInfo, contactNumber: contactNumber, cityId: cityId);
    }
    return cardPaymentObj != null;
  }

  Future<void> readCardPayment() async{
    paymentCards.value = await cardPaymentApiController.indexCardPayment();
  }

  Future<bool> updateCardPayment(BuildContext context ,{required CardPayment cardPayment}) async{
    bool update  = await cardPaymentApiController.updateCardPayment(context, cardPayment: cardPayment);
    int index = paymentCards.indexWhere((element) => element.id == cardPayment.id);
    if(index != -1) paymentCards[index] = cardPayment;
    return update;

  }


  Future<bool> deleteCardPayment(BuildContext context , {required int id}) async{
    bool deleted = await cardPaymentApiController.deleteCardPayment(context, id : id);
    int index = paymentCards.indexWhere((element) => element.id == id);
    if(index != -1) paymentCards.removeAt(index);
    return deleted;

  }

}