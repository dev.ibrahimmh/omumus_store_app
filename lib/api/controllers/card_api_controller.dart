import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:omumus_store_app/api/api_settings.dart';
import 'package:omumus_store_app/models/card_payment.dart';
import 'package:omumus_store_app/storage/user_shared_pref.dart';
import 'package:omumus_store_app/utils/helpers.dart';

class CardPaymentApiController{

  Future<List<CardPayment>> indexCardPayment() async {
    var url = Uri.parse(ApiSettings.CARD);
    var response = await http.get(url, headers: {
      HttpHeaders.authorizationHeader : UserSharedPref().getToken(),
      "X-Requested-With" : "XMLHttpRequest",
      "Connection": "keep-alive",
    });

    if(response.statusCode == 200){
      var jsonResponse = jsonDecode(response.body)['list'] as List;
      List<CardPayment> addresses = jsonResponse.map((e) => CardPayment.fromJson(e)).toList();
      return addresses;
    }
    return [];
  }


  Future<CardPayment?> addCardPayment(BuildContext context , {required String holderName , required String cardNumber, required String expData , required String cvvNum, required String type}) async{
    var url = Uri.parse(ApiSettings.CARD);
    var response = await http.post(url, body: {
      "holder_name" : holderName,
      "card_number" : cardNumber,
      "exp_date" : expData,
      "cvv" : cvvNum,
      "type": type,
    },
        headers: {
          "X-Requested-With" : "XMLHttpRequest",
          HttpHeaders.authorizationHeader : UserSharedPref().getToken(),
        }
    );
    // print(url);
    // print('responseCard: ${response.statusCode}');
    // print('sssss: ${response.body}');
    if(response.statusCode == 201){
      var cardResponse =  jsonDecode(response.body)['object'];

      Helpers.showSnackBar(context: context, message: jsonDecode(response.body)['message']);


      return CardPayment.fromJson(cardResponse);
    }else{
      Helpers.showSnackBar(context: context, message: jsonDecode(response.body)['message'], error: true);

      return null;
    }
  }


  Future<bool> updateCardPayment(BuildContext context , {required CardPayment cardPayment}) async{
    var url = Uri.parse(ApiSettings.CARD + "/" + cardPayment.id.toString());
    var response = await http.put(url, body: {
      "holder_name" : cardPayment.holderName,
      "card_number" : cardPayment.cardNumber,
      "exp_date" : cardPayment.expDate,
      "cvv" : cardPayment.cvv.toString(),
    },
        headers: {
          "X-Requested-With" : "XMLHttpRequest",
          HttpHeaders.authorizationHeader : UserSharedPref().getToken(),
        }
    );

    if(response.statusCode == 200){

      Helpers.showSnackBar(context: context, message: jsonDecode(response.body)['message']);

      return true;
    }else{
      Helpers.showSnackBar(context: context, message: jsonDecode(response.body)['message'], error: true);

      return false;
    }
  }


  Future<bool> deleteCardPayment(BuildContext context , {required int id}) async{
    var url = Uri.parse(ApiSettings.CARD + "/" + id.toString());
    var response = await http.delete(url, headers: {
      "X-Requested-With" : "XMLHttpRequest",
      HttpHeaders.authorizationHeader : UserSharedPref().getToken(),
    }
    );

    if(response.statusCode == 200){
      Helpers.showSnackBar(context: context, message: jsonDecode(response.body)['message']);

      return true;
    }else{
      Helpers.showSnackBar(context: context, message: jsonDecode(response.body)['message'], error: true);

      return false;
    }
  }

}