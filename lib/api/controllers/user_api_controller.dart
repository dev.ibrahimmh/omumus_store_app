import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omumus_store_app/api/api_settings.dart';
import 'package:http/http.dart' as http;
import 'package:omumus_store_app/models/User.dart';
import 'package:omumus_store_app/storage/user_shared_pref.dart';
import 'package:omumus_store_app/utils/helpers.dart';
class UserApiController{

  Future<User?> login(BuildContext context,String mobile, String password) async{

    var url = Uri.parse(ApiSettings.USER_LOGIN);
    var response = await http.post(url, body: {
      'mobile': mobile,
      'password': password,
    });
    print('response: ${response.statusCode}');

    if(response.statusCode == 200){
      // print('data: ');
      var userJsonData = jsonDecode(response.body)['data'];

      print('data: $userJsonData');
     return User.fromJson(userJsonData);
    }else{
      var jsonResponse = jsonDecode(response.body);
      Helpers.showSnackBar(context: context, message: jsonResponse['message'], error: true);
    }
    return null;
  }

  Future<bool> logout() async {
    Uri url = Uri.parse(ApiSettings.USER_LOGOUT);
    var response = await http.get(url, headers: {
      HttpHeaders.authorizationHeader: UserSharedPref().getToken()
    });
    if(response.statusCode == 200){
      UserSharedPref().logout();
      return true;
    }
    return false;
  }


  Future<bool> forgetPassword(BuildContext context,{required String mobile}) async{
    var url = Uri.parse(ApiSettings.USER_FORGET_PASSWORD);
    var response = await http.post(url,body: {'mobile': mobile});
    if(response.statusCode == 200){
      Helpers.showSnackBar(context: context, message: jsonDecode(response.body)['message']);
      Helpers.showSnackBar(context: context, message: 'Code: ${jsonDecode(response.body)['code']}');
      // print('Code: ${jsonDecode(response.body)['code']}');
      return true;
    }
    Helpers.showSnackBar(context: context, message: jsonDecode(response.body)['message'],error: true);
    return false;
  }


  Future<bool> resetPassword(BuildContext context,
      {required String password,required String mobile,required String code}) async {
    var url = Uri.parse(ApiSettings.USER_RESET_PASSWORD);
    var response = await http.post(url,body: {
      'mobile': mobile,
      'code': code,
      'password': password,
      'password_confirmation': password,
    });

    if(response.statusCode == 200){
      Helpers.showSnackBar(context: context, message: jsonDecode(response.body)['message'],error: false);
      return true;
    }
    Helpers.showSnackBar(context: context, message: jsonDecode(response.body)['message'],error: true);
    return false;
  }


  Future<bool> register(BuildContext context,{
    required String username,
    required String mobile,
    required String password,
    required String gender,
    required int cityId})async{
    var url = Uri.parse(ApiSettings.USER_REGISTER);
    var response = await http.post(url,body:{
      'name':username,
      'mobile':mobile,
      'password':password,
      'gender':gender,
      'STORE_API_KEY':'53c8ba1a-6975-43c4-8649-d681f0dd76bd',
      'city_id':cityId.toString(),
    });
    var decoded = jsonDecode(response.body);
    if(response.statusCode == 201){
      Get.snackbar('Success',decoded['message']);
      Helpers.showSnackBar(context: context, message: 'Code Number :' + decoded['code'].toString());
      print(decoded['code']);
      return true;
    }
    Get.snackbar('Error',decoded['message']);
    return false;
  }

  Future<bool> activate({required String mobile,required String code})async{
    final url = Uri.parse(ApiSettings.USER_ACTIVATE);
    final response = await http.post(url,body:{'mobile':mobile,'code':code});
    final decoded = jsonDecode(response.body);
    if(response.statusCode==200){
      Get.snackbar('Success', decoded['message'],duration: Duration(milliseconds: 1500));
      // await Future.delayed(Duration(seconds: 2),(){});
      return true;
    }else{
      Get.snackbar('Error', decoded['message']);
      return false;
    }
  }

}