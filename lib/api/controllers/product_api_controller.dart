import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:omumus_store_app/models/product.dart';
import 'package:omumus_store_app/models/product_details.dart';
import 'package:omumus_store_app/storage/user_shared_pref.dart';
import 'package:omumus_store_app/utils/helpers.dart';

import '../api_settings.dart';

class ProductApiController{

  Future<List<Product>> products({required int id}) async {
    var url = Uri.parse(ApiSettings.PRODUCTS_LIST + '/$id');
    var response = await http.get(url,headers: {
      HttpHeaders.authorizationHeader: UserSharedPref().getToken()
    });
    if(response.statusCode == 200) {
      var jsonResponse = jsonDecode(response.body);
      print('we are here');
      var productJsonData = jsonResponse['list'] as List ;
      // print('data $categoryJsonData');
      return productJsonData.map((jsonObject) => Product.fromJson(jsonObject)).toList();
    }
    return [];
  }

  Future<ProductDetails?> productsDetails(int id) async {
    var url = Uri.parse(ApiSettings.PRODUCTS_DETAILS + '/$id');
    var response = await http.get(url,headers: {
      HttpHeaders.authorizationHeader: UserSharedPref().getToken()
    });
    if(response.statusCode == 200) {
      var jsonResponse = jsonDecode(response.body)['object'];
      print('we are here');
      // print('data $jsonResponse');
      return ProductDetails.fromJson(jsonResponse);
    }
    return null;
  }


  Future<List<Product>> getFavoriteProducts() async {
    var url = Uri.parse(ApiSettings.FAVORITE_PRODUCTS);
    var response = await http.get(url ,headers: {
      'Accept':'application/json',
      HttpHeaders.authorizationHeader: UserSharedPref().getToken(),
      'X-Requested-With': 'XMLHttpRequest',
    });
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['list'] as List;
      data.map((e) => Product.fromJson(e)).toList();
      return data.map((jsonObject) => Product.fromJson(jsonObject)).toList();
    }
    return [];
  }


  Future<bool> addFavoriteProducts(BuildContext context, {required int id}) async {
    var url = Uri.parse(ApiSettings.FAVORITE_PRODUCTS);
    var response = await http.post(
        url,
        body: {'product_id': id.toString()},
        headers: {HttpHeaders.authorizationHeader: UserSharedPref().getToken(),
          'X-Requested-With': 'XMLHttpRequest',
          'lang': UserSharedPref().languageCode
        });
    if (response.statusCode == 200) {
      Helpers.showSnackBar(context: context,
          message: jsonDecode(response.body)['message'], error: false);
      return true;
    } else if (response.statusCode != 500) {
      Helpers.showSnackBar(context: context,
          message: jsonDecode(response.body)['message'], error: true);
    }else{
      Helpers.showSnackBar(context: context, message: 'Unable to perform your request now!', error: true);

    }
    return false;
  }

  Future<bool> productRate(BuildContext context, {required int id,required double ratting}) async {
    var url = Uri.parse(ApiSettings.RATTING_PRODUCTS);
    var response = await http.post(
        url,
        body: {'product_id': id.toString(), 'rate' : ratting.toString()},
        headers: {HttpHeaders.authorizationHeader: UserSharedPref().getToken(),
          'X-Requested-With': 'XMLHttpRequest',
          'lang': UserSharedPref().languageCode
        });
    print('response ${response.statusCode}');
    if (response.statusCode == 200) {
        Helpers.showSnackBar(context: context,
        message: jsonDecode(response.body)['message'], error: false);

      return true;
    } else if (response.statusCode != 500) {
        Helpers.showSnackBar(context: context,
        message: jsonDecode(response.body)['message'], error: true);
        print('true ibrahim');
    }else{
        Helpers.showSnackBar(context: context, message: 'Unable to perform your request now!', error: true);
    }
    return false;
  }



}