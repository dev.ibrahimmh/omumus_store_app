import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:omumus_store_app/api/api_settings.dart';
import 'package:omumus_store_app/models/category.dart';
import 'package:omumus_store_app/models/sub_category.dart';
import 'package:omumus_store_app/storage/user_shared_pref.dart';

class CategoryApiController{

  Future<List<Category>> categories() async {
    var url = Uri.parse(ApiSettings.CATEGORIES_LIST);
    var response = await http.get(url,headers: {
      HttpHeaders.authorizationHeader: UserSharedPref().getToken()
    });
    // print('response ${response.statusCode}');
    //     // print('response ${response.body}');
    //     // print('url $url');
    if(response.statusCode == 200) {
      var jsonResponse = jsonDecode(response.body);
      print('we are here');
      var categoryJsonData = jsonResponse['list'] as List ;
      // print('data $categoryJsonData');
      return categoryJsonData.map((jsonObject) => Category.fromJson(jsonObject)).toList();
    }
    return [];
  }



  Future<List<SubCategory>> subcategories({required int id}) async {
    var url = Uri.parse(ApiSettings.CATEGORIES_LIST + '/$id');
    var response = await http.get(url,headers: {
      HttpHeaders.authorizationHeader: UserSharedPref().getToken()
    });
    // print('response ${response.statusCode}');
    //     // print('response ${response.body}');
    //     // print('url $url');
    if(response.statusCode == 200) {
      var jsonResponse = jsonDecode(response.body);
      print('we are here');
      var subcategoryJsonData = jsonResponse['list'] as List ;
      // print('data $categoryJsonData');
      return subcategoryJsonData.map((jsonObject) => SubCategory.fromJson(jsonObject)).toList();
    }
    return [];
  }


}