import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:omumus_store_app/api/api_settings.dart';
import 'package:omumus_store_app/models/city.dart';

class CityApiController{

  Future<List<City>> cities() async {
    var url = Uri.parse(ApiSettings.USER_CITIES);
    var response = await http.get(url, headers: {
      'Accept': 'application/json'
    });
    print('response: ${response.statusCode}');
    print('url: $url');
    if(response.statusCode == 200) {
      var cityJsonData = jsonDecode(response.body)['list'] as List;
      return cityJsonData.map((jsonObject) => City.fromJson(jsonObject)).toList();
    }
    return [];
  }
}