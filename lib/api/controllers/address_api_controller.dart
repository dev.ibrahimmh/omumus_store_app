import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:omumus_store_app/api/api_settings.dart';
import 'package:omumus_store_app/models/address.dart';
import 'package:omumus_store_app/storage/user_shared_pref.dart';
import 'package:omumus_store_app/utils/helpers.dart';



class AddressApiController{

  Future<List<Address>> indexAddresses() async {
    var url = Uri.parse(ApiSettings.ADDRESS);
    var response = await http.get(url, headers: {
      HttpHeaders.authorizationHeader : UserSharedPref().getToken(),
      "X-Requested-With" : "XMLHttpRequest",
      "Connection": "keep-alive",
    });

    if(response.statusCode == 200){
      var jsonResponse = jsonDecode(response.body)['list'] as List;
      List<Address> addresses = jsonResponse.map((e) => Address.fromJson(e)).toList();
      return addresses;
    }
    return [];
  }


  Future<Address?> addAddress(BuildContext context , {required String addressName , required String addressInfo, required String contactNumber , required int cityId}) async{
    var url = Uri.parse(ApiSettings.ADDRESS);
    var response = await http.post(url, body: {
      "name" : addressName,
      "info" : addressInfo,
      "contact_number" : contactNumber,
      "city_id" : cityId.toString(),
    },
        headers: {
          "X-Requested-With" : "XMLHttpRequest",
          HttpHeaders.authorizationHeader : UserSharedPref().getToken(),
        }
    );

    if(response.statusCode == 201){
      var addressResponse =  jsonDecode(response.body)['object'];

      Helpers.showSnackBar(context: context, message: jsonDecode(response.body)['message']);


      return Address.fromJson(addressResponse);
    }else{
      Helpers.showSnackBar(context: context, message: jsonDecode(response.body)['message'], error: true);

      return null;
    }
  }


  Future<bool> updateAddress(BuildContext context , {required Address address}) async{
    var url = Uri.parse(ApiSettings.ADDRESS + "/" + address.id.toString());
    var response = await http.put(url, body: {
      "name" : address.name,
      "info" : address.info,
      "contact_number" : address.contactNumber,
      "city_id" : address.cityId.toString(),
    },
        headers: {
          "X-Requested-With" : "XMLHttpRequest",
          HttpHeaders.authorizationHeader : UserSharedPref().getToken(),
        }
    );

    if(response.statusCode == 200){

      Helpers.showSnackBar(context: context, message: jsonDecode(response.body)['message']);

      return true;
    }else{
      Helpers.showSnackBar(context: context, message: jsonDecode(response.body)['message'], error: true);

      return false;
    }
  }


  Future<bool> deleteAddress(BuildContext context , {required int id}) async{
    var url = Uri.parse(ApiSettings.ADDRESS + "/" + id.toString());
    var response = await http.delete(url, headers: {
      "X-Requested-With" : "XMLHttpRequest",
      HttpHeaders.authorizationHeader : UserSharedPref().getToken(),
    }
    );

    if(response.statusCode == 200){

      Helpers.showSnackBar(context: context, message: jsonDecode(response.body)['message']);

      return true;
    }else{
      Helpers.showSnackBar(context: context, message: jsonDecode(response.body)['message'], error: true);

      return false;
    }
  }

}