 import 'dart:convert';
import 'dart:io';

import 'package:get/get.dart';
import 'package:omumus_store_app/api/api_settings.dart';
import 'package:omumus_store_app/models/home/base_home.dart';

 import 'package:http/http.dart' as http;
import 'package:omumus_store_app/storage/user_shared_pref.dart';

 class HomeController extends GetxController{
   Rxn<Home> HomeOpj = Rxn<Home>();

   Future<bool> indexHome()async{
     final url = Uri.parse(ApiSettings.HOME);
     final response = await http.get(url,headers:{
       HttpHeaders.authorizationHeader: UserSharedPref().getToken(),
       'X-Requested-With':'XMLHttpRequest'
     });
     final decoded = jsonDecode(response.body);
     if(response.statusCode==200){
       final homeopj = decoded['data'];
       HomeOpj.value = Home.fromJson(homeopj);
       HomeOpj.refresh();
       return true;
     }else{
       HomeOpj.value = null;
       return false;
     }
   }
 }

