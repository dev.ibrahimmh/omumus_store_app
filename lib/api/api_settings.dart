class ApiSettings{

  static const _API_BASE_URL = "https://smart-store.mr-dev.tech/api/";


  static const USER_LOGIN = _API_BASE_URL + 'auth/login';
  static const USER_LOGOUT = _API_BASE_URL + 'auth/logout';
  static const USER_FORGET_PASSWORD = _API_BASE_URL + 'auth/forget-password';
  static const USER_RESET_PASSWORD = _API_BASE_URL + 'auth/reset-password';
  static const USER_REGISTER = _API_BASE_URL + 'auth/register';
  static const USER_ACTIVATE = _API_BASE_URL + 'auth/activate';
  static const USER_CITIES = _API_BASE_URL + 'cities';


  static const CATEGORIES_LIST = _API_BASE_URL + 'categories';

  static const HOME = _API_BASE_URL + 'home';


  static const PRODUCTS_LIST = _API_BASE_URL + 'sub-categories';
  static const PRODUCTS_DETAILS = _API_BASE_URL + 'products';
  static const FAVORITE_PRODUCTS = _API_BASE_URL + 'favorite-products';
  static const RATTING_PRODUCTS = _API_BASE_URL + 'products/rate';


  static const ADDRESS = _API_BASE_URL + 'addresses';
  static const CARD = _API_BASE_URL + 'payment-cards';


}