import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:omumus_store_app/screens/auth_screens/forget_password.dart';
import 'package:omumus_store_app/screens/auth_screens/sign_in_screen.dart';
import 'package:omumus_store_app/screens/auth_screens/sign_up_screen.dart';
import 'package:omumus_store_app/screens/bottom_navegationBar/main_screen.dart';
import 'package:omumus_store_app/screens/on_boarding_screen.dart';
import 'package:omumus_store_app/screens/splash_screen.dart';
import 'package:omumus_store_app/storage/db_provider.dart';
import 'package:omumus_store_app/storage/user_shared_pref.dart';

import 'getx_controller/city_getx_controller.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await UserSharedPref().initSharedPref();
  await DBProvider().initDatabase();
  runApp(MainApp());
}

class MainApp extends StatelessWidget {
  CityGetxController city = Get.put(CityGetxController());
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      // home: SplashScreen(),
      initialRoute: '/splash_screen',
      getPages: [
        GetPage(name: '/splash_screen', page: ()=> SplashScreen()),
        GetPage(name: '/on_boarding_screen', page: ()=> OnBoardingScreen()),
        GetPage(name: '/sign_in_screen', page: ()=> SignInScreen()),
        GetPage(name: '/sign_up_screen', page: ()=> SignUpScreen()),
        GetPage(name: '/forget_password', page: ()=> ForgetPassword()),
        GetPage(name: '/main_screen', page: ()=> MainScreen(indexid: 0,)),
      ],
    );
  }
}
